import {
  AxesHelper,
  DirectionalLight,
  CameraHelper,
  Mesh,
  MeshBasicMaterial,
  PerspectiveCamera,
  Scene,
  BoxGeometry,
  WebGLRenderer,
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js';
import {
  GUI
} from '../node_modules/three/examples/jsm/libs/dat.gui.module.js'
//创建场景
const scene = new Scene()

//创建摄像机
const fov = 40 // 视野范围
const aspect = 2 // 相机默认值 画布的宽高比
const near = 0.1 // 近平面
const far = 1000 // 远平面
const camera = new PerspectiveCamera(fov, aspect, near, far)
camera.position.set(-20, 10, 20)
camera.lookAt(0, 0, 0)

//渲染器
const renderer = new WebGLRenderer()
//设置渲染器大小和颜色
renderer.setSize(window.innerWidth, window.innerHeight)
//挂载dom
document.body.appendChild(renderer.domElement)
//将场景和摄像机放入渲染器当中
renderer.render(scene, camera)

//创建几何体
const cubeSize = 4;
const geometry = new BoxGeometry(cubeSize, cubeSize, cubeSize)
const materia = new MeshBasicMaterial({
  color: '#8f4b2e'
})
//设置位置
geometry.center()
const cube = new Mesh(geometry, materia)
cube.position.y = 2
scene.add(cube)
//添加三维轴(调整线的长段)
const axes = new AxesHelper(50);
scene.add(axes)
//修改轴的颜色
// axes.setColors(0x00ffff,0x002299,0xff0000)

//增加灯光
const color = 0x8f4b2e;
const intensity = 1;
//方向光
const light = new DirectionalLight(color, intensity)
light.position.set(0, 10, 0)
light.target.position.set(-5, 0, 0)
scene.add(light)
scene.add(light.target)

//鼠标控制
function animate() {
  renderer.render(scene, camera)
  requestAnimationFrame(animate)
}
animate()
const controls = new OrbitControls(camera, renderer.domElement)

//点击移动相机
document.querySelector('#onPosition').addEventListener('click', function () {
  camera.position.set(0, 20, 20)
})

//点击 修改相机 视野范围
document.querySelector('#onView').addEventListener('click', function () {
  camera.fov = 20;
  //每次更改视野范围，启动updateProjectionMatrix
  camera.updateProjectionMatrix()
})

//辅助相机(两个相机)
//刷新
const long = 15;
const short = 8;
const big = 800;
const camera1 = new PerspectiveCamera(long, 2, short, big)
camera1.position.set(0, 5, 20)
camera1.lookAt(0, 0, 0)
const cameraHelper = new CameraHelper(camera1)
scene.add(cameraHelper)
let cameraBol = true;
//点击切换相机
document.querySelector('#cameraBol').addEventListener('click', () => {
  cameraBol = !cameraBol
  console.log(cameraBol)
  render()
})

//渲染
function render() {
  cameraHelper.update()
  if (cameraBol) {
    let  long = 15;
    let short = 8;
    let big = 800;
    const camera1 = new PerspectiveCamera(long, 2, short, big)
    camera1.position.set(0, 5, 20)
    camera1.lookAt(0, 0, 0)
    scene.add(cameraHelper)
    renderer.render(scene, camera1)
  } else {
    renderer.render(scene, camera)
    scene.remove(cameraHelper)
  }
}

function updateCamera() {
 long+=camera1.fov
  camera1.updateProjectionMatrix()
   //通过requestAnimationFrame方法在特定时间间隔重新渲染场景
   requestAnimationFrame(updateCamera);
}
//初始化,将这个对象传递给gui对象，让它的数值变成能够控制的
var gui = new GUI(); //新建gui控制组件
gui.add(camera1, 'fov', 1, 180)
gui.add(camera1, 'near', 0.1, 10)
gui.add(camera1, 'far', 100, 1000)
updateCamera()

//修改全局position
class PositionGUI {
  constructor(obj, name) {
    this.obj = obj
    this.name = name
  }
  get modify() {
    return this.obj[this.name]
  }
  set modify(v) {
    this.obj[this.name] = v
  }
}
const folder = gui.addFolder('全局Position')
folder.add(new PositionGUI(camera.position, 'x'), 'modify', -20, 0).name('x')
folder.add(new PositionGUI(camera.position, 'y'), 'modify', 0, 200).name('y')
folder.add(new PositionGUI(camera.position, 'z'), 'modify', 0, 200).name('z')