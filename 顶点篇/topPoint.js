import {
  OrbitControls
} from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
import {
  AmbientLight,
  AxesHelper,
  BufferAttribute,
  BufferGeometry,
  Color,
  DoubleSide,
  Fog,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  PerspectiveCamera,
  PointLight,
  Points,
  PointsMaterial,
  Scene,
  VertexColors,
  WebGLRenderer
} from "../node_modules/three/build/three.module.js";

const scene = new Scene()
const camera = new PerspectiveCamera(40, 2, 0.1, 1000)
camera.position.set(70, 90, 400)
camera.lookAt(0, 0, 0)
const renderer = new WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls = new OrbitControls(camera, renderer.domElement)
//-------------刷新-----------------
function render(time) {
  renderer.render(scene, camera)
  time *= 0.05
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

scene.background = new Color(0x87ceeb)
// scene.fog=new Fog(0x87ceeb,1,11)

const axes = new AxesHelper(100)
scene.add(axes)
const vertices = new Float32Array([
  0, 0, 0,
  50, 0, 0,
  0, 100, 0,
  0, 0, 10,
  0, 0, 100,
  50, 0, 10,
])


//---------自定义模型--------------
const geometry = new BufferGeometry()
geometry.attributes.position = new BufferAttribute(vertices, 3)

//-------顶点颜色-------------------
const colors = new Float32Array([
  1, 0, 0, //顶点1颜色
  0, 1, 0, //顶点2颜色
  0, 0, 1, //顶点3颜色
  1, 1, 0, //顶点4颜色
  0, 1, 1, //顶点5颜色
  1, 0, 1, //顶点6颜色
])
//生成面
const material = new MeshBasicMaterial({
  color: 0x0000ff, //三角面颜色
  side:DoubleSide //两面可见
})
const mesh = new Mesh(geometry)
scene.add(mesh)
//生成点
geometry.attributes.color = new BufferAttribute(colors, 3)
const material2 = new PointsMaterial({
  vertexColors: VertexColors,
  size: 10.0
})
//只显示点(Point模型代替网络模型)
// const points=new Points(geometry,material2)
//显示面，随着点的颜色不同，形成渐变色
const points=new Mesh(geometry,material2)
scene.add(points)

//--------------第二个图案-----------------------------------
//顶点法向量geometry.attributes.normal
const geometry2=new BufferGeometry()
geometry2.attributes.position=new BufferAttribute(vertices,3)
const material3=new MeshPhongMaterial({
  color:0x0000ff,
  side:DoubleSide
})
const mesh2=new Mesh(geometry2,material3)
mesh2.position.x=80
scene.add(mesh2)

//环境光
const ambient=new AmbientLight(0x444444)
scene.add(ambient)

//点光源
const pointlight=new PointLight(0xffffff)
pointlight.position.set(200,400,300)
scene.add(pointlight)

//图案是黑的，因为用BufferGeometry的话，如果需要用普通材质，想要看到光照效果，必须设置顶点法向量。
const normals = new Float32Array([
  0, 0, 1, //顶点1法向量
  0, 0, 1, //顶点2法向量
  0, 0, 1, //顶点3法向量
  0, 1, 0, //顶点4法向量
  0, 1, 0, //顶点5法向量
  0, 1, 0, //顶点6法向量
]);
geometry2.attributes.normals=new BufferAttribute(normals,3)

