//创建场景
var scene =new THREE.Scene();

//光源设置
const intensity=3;
var Light = new THREE.PointLight(0xffffff,intensity);
Light.position.set(800,800,800);//点光源位置(xyz确定方向)
scene.add(Light);//点光源添加到场景中

//环境光
var ambient =new THREE.AmbientLight(0xffff00);
scene.add(ambient)

//创建一个透视相机（视角，横纵比（宽高比），近面的距离，远面的距离）
const fov=45;//视野范围 (值越大，看的越小)
const aspect=window.innerWidth / window.innerHeight;//相机的默认值  画布的宽高比
const near=0.1;//近平面
const far=1000;//原平面（低于近平面或者远平面都会看不见）
var camera=new THREE.PerspectiveCamera(fov,aspect,near,far);

//创建并使用渲染器
var renderer=new THREE.WebGLRenderer();
//设置渲染器大小（设置渲染区域的大小）
renderer.setSize(window.innerWidth,window.innerHeight);
//把创造好的canvas添加到页面上
document.body.appendChild(renderer.domElement);
//将场景和摄像机放入渲染器当中
renderer.render(scene,camera)

//创建一个几何体
//长宽高
var geometry=new THREE.BoxGeometry(1,1,1);
//创建一个网格基础材质，并设置他的颜色
var material=new THREE.MeshBasicMaterial({color:0x002299});
//设置网格
var cube =new THREE.Mesh(geometry,material);
cube.rotation.x=0.3;
cube.rotation.y=0.5;
scene.add(cube);
camera.position.z=10;

// //创建网格、球体，并加入场景
// //创建球体
// const radius=2;//半径
// const widthSegment=6;//经度上的切片数
// const heightSegment=6;//纬度上的切片数
// const sphereGeometry=new THREE.SphereGeometry(radius,widthSegment,heightSegment)
// //创建材质(具有镜面高光的光泽表面材质)
// const sunMaterial=new THREE.MeshPhongMaterial({color:0x002299,emissive:0xffff00})
// //将球体和材质，带入到网格当中
// const sunMesh=new THREE.Mesh(sphereGeometry,sunMaterial)
// camera.position.z=10
// scene.add(sunMesh)

//改变场景或者摄像机，重新渲染（显得非常麻烦）
renderer.render(scene,camera)
// //解决方案(每隔60/1秒执行)
// function animate(){
//   requestAnimationFrame(animate);
//   // cube.rotation.x+=0.03;
//   // cube.rotation.y+=0.03;
//   sunMesh.rotation.x+=0.03;
//   sunMesh.rotation.y+=0.03;
//   sunMesh.rotation.z+=0.03;
//   // camera.position.z+=0.02
//   // camera.position.x+=0.02

//   renderer.render(scene,camera);
// }
// animate();
// window.onresize=function(){
//   renderer.setSize(window.innerWidth,window.innerHeight);
//   camera.aspect=window.innerWidth / window.innerHeight;
//   camera.updateProjectionMatrix();
// }