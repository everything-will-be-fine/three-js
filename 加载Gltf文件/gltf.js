import {
  DirectionalLight,
  HemisphereLight,
  Color,
  Scene,
  PerspectiveCamera,
  WebGLRenderer
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
import {
  GLTFLoader
} from '../node_modules/three/examples/jsm/loaders/GLTFLoader.js'
//----------基础模板-------------------------------
const scene = new Scene()
scene.background = new Color('black')
const camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.set(0, 0, 10)
camera.lookAt(0, 0, 0)

const renderer = new WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
//renderer.setClearColor(0xffffff)
document.body.appendChild(renderer.domElement)
renderer.render(scene, camera)

const controls = new OrbitControls(camera, renderer.domElement)
//----------------渲染------------------
function render() {
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-----------灯光----------------------
{
  //方向光
  const color = 0xffffff
  const intensity = 1
  const light = new DirectionalLight(color, intensity)
  light.position.set(0, 10, 0)
  light.target.position.set(-5, 0, 0)
  scene.add(light)
  scene.add(light.target)
} {
  //半球光
  const skyColor = 0xb1e1ff
  const groundColor = 0xffffff
  const intensity = 1
  const light = new HemisphereLight(skyColor, groundColor, intensity)
  scene.add(light)
}

//-----------引入gltf插件-----------------------
const gltfLoader = new GLTFLoader()
gltfLoader.ktx2Loader('./images/Duck.gltf', (gltf) => {
  const root = gltf.scene
  console.log(root)
  scene.add(root)
})