
import {
  WebGLCubeRenderTarget,
  CubeTextureLoader,
  MeshBasicMaterial,
  TextureLoader,
  BoxGeometry,
  WebGLRenderer,
  PerspectiveCamera,
  Scene,
  Mesh,
  BackSide
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'

//-----------基础模板--------------
const scene = new Scene()
const camera = new PerspectiveCamera(40, 2, 0.1, 1000)
camera.position.set(10, 3, 0)
camera.lookAt(0, 0, 0)

const canvas = document.querySelector('#c2d')
const renderer = new WebGLRenderer({
  canvas
})

//----------刷新--------------------
function render(time) {
  time *= 0.05
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)
const controls = new OrbitControls(camera, renderer.domElement)

//-------------添加几何体--------------------
{
  var pictures = [
    './images/images/skyBox_02.gif',
    './images/images/skyBox_05.gif',
    './images/images/skyBox_06.gif',
    './images/images/skyBox_07.gif',
    './images/images/skyBox_08.gif',
    './images/images/skyBox_10.gif',
  ]
  //-------------------第一种-------------------------
  // const loader=new TextureLoader()
  // const skyGeometry=new BoxGeometry(5000,5000,5000)
  // const material=[]
  // for(let i=0;i<pictures.length;i++){
  //   material.push(
  //     new MeshBasicMaterial({
  //       map:loader.load(pictures[i]),
  //       side:BackSide
  //     })
  //   )
  //   const skyBox=new Mesh(skyGeometry,material)
  //   scene.add(skyBox)
  // }
  //--------------第二种方法加载切片-----------------------------------
  // const loader = new CubeTextureLoader()
  // const texture = loader.load([
  //   './images/images/skyBox_02.gif',
  //   './images/images/skyBox_05.gif',
  //   './images/images/skyBox_06.gif',
  //   './images/images/skyBox_07.gif',
  //   './images/images/skyBox_08.gif',
  //   './images/images/skyBox_10.gif',
  // ])
  // scene.background = texture
}

//------------------------添加360°全景图----------------------------

{
  const loader=new TextureLoader()
  const texture=loader.load('./images//images//360All.webp',()=>{
    const rt=new WebGLCubeRenderTarget(texture.image.height)
    rt.fromEquirectangularTexture(renderer,texture)
    scene.background=rt.texture
  })
}
