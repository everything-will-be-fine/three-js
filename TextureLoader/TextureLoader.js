
import {  
  SphereGeometry,
  MeshPhongMaterial,
  TextureLoader,
  Vector2,
  Mesh,
  PerspectiveCamera,
  Scene,
  WebGL1Renderer,
  PlaneGeometry,
  AmbientLight,
  SpotLight,
  GridHelper,
  MeshStandardMaterial
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'

//创建场景
const scene = new Scene()

//创建摄像机
const camera = new PerspectiveCamera(45, window.innerHeight / window.innerWidth, 0.1, 1000)
//创建位置
camera.position.set(100, 30, 500)
//将摄像机的方向对准场景的中心点
camera.lookAt(scene.position)

//创建渲染器
const renderer = new WebGL1Renderer()
renderer.setClearColor(0xffffff)
renderer.setSize(window.innerWidth, window.innerHeight)
//挂载dom
document.body.appendChild(renderer.domElement)
//渲染器渲染阴影
renderer.shadowMap.enabled = true;
renderer.render(scene, camera)

//创建一个平面图
const planeGeometry = new PlaneGeometry(2000, 2000)
//平面材质
const planeMaterial = new MeshPhongMaterial({
  color: 0xffffff,
  depthWrite: false
})
//平面
const plane = new Mesh(planeGeometry, planeMaterial)
plane.receiveShadow = true;
plane.rotation.x = -0.5 * Math.PI;
plane.position.set(15, 0, 0)
scene.add(plane)

//网格辅助，格子越密集，第二个参数越大
const grid = new GridHelper(2000, 200, 0xff0000, 0x000000)
grid.material.opacity = 0.2;
grid.material.transparent = true;
scene.add(grid)

//生成环境光
const ambientLight = new AmbientLight(0xffffff)
scene.add(ambientLight)
renderer.render(scene, camera)

//聚光灯
const spot = new SpotLight(0x00FA9A)
spot.position.set(-20, 70, -65)
spot.castShadow = true;
//Vector2 表示二维向量（x、y的值）
spot.shadow.mapSize = new Vector2(1024, 1024);
//照射越长
spot.shadow.camera.far = 140;
//照射偏移
spot.shadow.camera.near = 30;
scene.add(spot)
renderer.render(scene, camera)

//创建纹理
const loader = new TextureLoader()
//创建球体
const earthGeom = new SphereGeometry(20, 200, 200)
loader.load('./images/bg1.webp', (texture) => {
  //设置球体材料
  const earthMat = new MeshStandardMaterial({
    map: texture,
  })
  //创建网格
  const earthMesh = new Mesh(earthGeom,earthMat)
  //接受阴影
  earthMesh.castShadow=true;
  earthMesh.position.y=50;
  scene.add(earthMesh)
})

function render() {
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
render()
const controls = new OrbitControls(camera, renderer.domElement)