import {RepeatWrapping,Mesh,MeshPhongMaterial, PerspectiveCamera, Scene, WebGLRenderer, PlaneGeometry, TextureLoader, MeshLambertMaterial, DoubleSide, DirectionalLight, Color, Fog, SphereGeometry, AudioListener, AudioLoader, PositionalAudio, Group, BoxGeometry, AudioAnalyser } from "../node_modules/three/build/three.module.js"
import{OrbitControls}from '../node_modules/three/examples/jsm/controls/OrbitControls.js'

const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,7000)
camera.position.set(100,100,700)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer()
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls=new OrbitControls(camera,renderer.domElement)


//几何体
const Geometry=new SphereGeometry(50,40,40)
const textureFootball=new TextureLoader().load('./video/egg.webp')
const material=new MeshPhongMaterial({
  map:textureFootball
})
const cube=new Mesh(Geometry,material)
cube.position.y=50
scene.add(cube)

//草坪
const plane=new PlaneGeometry(1000,1000)
const textureGrass=new TextureLoader().load('./video/grass.webp')
//设置纹理重复样式
textureGrass.wrapS=RepeatWrapping
textureGrass.wrapT=RepeatWrapping
textureGrass.repeat.set(10,10)

const materialPlane=new MeshLambertMaterial({
  map:textureGrass,
  side:DoubleSide
})
const mesh=new Mesh(plane,materialPlane)
scene.add(mesh)
mesh.rotateX(-Math.PI/2)

//灯光
{
  const color=0xffffff
  const intensity=2
  const light=new DirectionalLight(color,intensity)
  light.position.set(-1,2,5)
  scene.add(light)
}
//雾气
{
  const color='lightblue'
  scene.background=new Color(color)
  //scene.fog=new Fog(color,1)
}


//------------------音乐可视化---------------------
const group=new Group()
let N=128;//控制音频分析器返回频率数据数量
for(let i=0;i<N/2;i++){
  var box=new BoxGeometry(10,50,10)//创建一个几何体
  var materialMusic=new MeshPhongMaterial({
    color:0x0000ff
  })//材质对象
  var Music=new Mesh(box,materialMusic)
  //长方体间隔20，整体居中
  Music.position.set(20*i-N/2*10,100,0)
  group.add(Music)
}
scene.add(group)

//声明一个分析器变量
var analyser=null;
//渲染函数
function render(){
  renderer.render(scene,camera)
  requestAnimationFrame(render)
  if(analyser){
    //获得频率数据N个
    var arr=analyser.getFrequencyData()
   // console.log(arr)
    //遍历组对象，每个网格子对象设置一个对应的频率数据
    group.children.forEach((elem,index)=>{
      elem.scale.y=arr[index]/80
    })
  }
}
requestAnimationFrame(render)

//----------------加入背景音乐------------------
//创建一个虚拟聆听者
const listener=new AudioListener()
//监听者绑定到相机对象
camera.add(listener)
//创建一个位置音频对象，监听者作为参数，音频和监听者关联
const PosAudio=new PositionalAudio(listener)
//音源绑定到一个网格模型上
cube.add(PosAudio)
//创建一个音频加载器
const audioLoader=new AudioLoader()
audioLoader.load('./video/BGM.mp3',function(AudioBuffer){
  console.log(AudioBuffer)
  //音频缓冲区对象关联到音频对象audio
  PosAudio.setBuffer(AudioBuffer)
  PosAudio.setLoop(true)
  PosAudio.setVolume(1)//音量
  PosAudio.setRefDistance(200)//参数值越大，声音越大
  PosAudio.play()
  //音频分析器和音频绑定，可以实时采集音频时域数据进行快速傅里叶变换
   analyser=new AudioAnalyser(PosAudio,2*N)
})