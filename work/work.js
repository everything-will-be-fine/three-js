//创建数组表
const arr = ['./pic/部分肠管.stl', './pic/部分胃.stl', './pic/胆囊.stl', './pic/胆总管.stl', './pic/动脉血管  .stl', './pic/肝静脉血管 .stl',
  './pic/肝内胆管结石可能.stl', './pic/肝脏.stl', './pic/肝脏（去尾状叶）.stl', './pic/肝脏（尾状叶）.stl', './pic/肝脏（右半肝）.stl', './pic/肝脏（右后叶）.stl', './pic/肝脏（右前叶）.stl',
  './pic/肝脏（左半肝）.stl', './pic/肝脏（左外叶）.stl', './pic/肝脏（VI段）.stl',
]
const arr2 = ['./pic/肝脏（II段）.stl', './pic/肝脏（III段）.stl', './pic/肝脏（IV段）.stl', './pic/肝脏（VIII段）.stl', './pic/肝脏（VII段）.stl',
  './pic/肝脏（V段）.stl', './pic/扩张胆管 .stl', './pic/淋巴结.stl', './pic/门静脉血管 .stl', './pic/囊肿 .stl', './pic/脾脏.stl', './pic/下腔静脉.stl',
  './pic/胰管 .stl', './pic/胰脏.stl', './pic/占位.stl'
]


for (let i = 0; i < arr.length; i++) {
  let left = document.getElementById('left')
  let elem = document.createElement("div")
  //添加属性名
  elem.setAttribute('class', 'small')
  left.appendChild(elem)
  //开始解析stl文件
  const loader = new THREE.STLLoader();
  loader.load(`${arr[i]}`, geometry => {
    console.log(geometry)
    //创建场景
    const scene = new THREE.Scene();
    //创建相机
    var width = window.innerWidth;
    var height = window.innerHeight;
    var k = width / height;
    var s = 50;
    var camera = new THREE.OrthographicCamera(-s * k, s * k, s, -s, -2000, 2000);
    camera.position.set(100, 100, 120);
    camera.lookAt(scene.position);
    //创建渲染器
    const renderer = new THREE.WebGL1Renderer({
      antialias: true
    });
    //挂载dom
    let small = document.getElementsByClassName('small')[i];
    small.appendChild(renderer.domElement)
    renderer.setSize(100, 100)
    //把相机和场景渲染进去
    renderer.render(scene, camera)

    //添加材料
    const materia = new THREE.MeshBasicMaterial({
      color: 0x002299
    })
    //几何体设置位置
    geometry.center();
    geometry.scale(0.5, 0.5, 0.5)
    //创建网格
    const cube = new THREE.Mesh(geometry, materia)
    //将网格放入渲染器当中
    scene.add(cube)
    //重新渲染
    renderer.render(scene, camera)
    console.log(scene.toJSON())
    //自己旋转
    function animate(){
      cube.rotation.x+=0.03;
      cube.rotation.y+=0.03;
      requestAnimationFrame(animate)
      renderer.render(scene,camera)
    }
    animate()
  })
}

// for (let j = 0; j < arr2.length; j++) {
//   let right = document.getElementById('right')
//   let elem2 = document.createElement("div")
//   //添加属性名
//   elem2.setAttribute('class', 'small2')
//   right.appendChild(elem2)
//   const loader2 = new THREE.STLLoader();
//   loader2.load(`${arr2[j]}`, geometry2 => {
//     console.log(geometry2)
//     //创建场景
//     const scene2 = new THREE.Scene();
//     //创建相机
//     var width2 = window.innerWidth;
//     var height2 = window.innerHeight;
//     var k = width2 / height2;
//     var s = 50;
//     var camera2 = new THREE.OrthographicCamera(-s * k, s * k, s, -s, -2000, 2000);
//     camera2.position.set(100, 100, 120);
//     camera2.lookAt(scene2.position);
//     //创建渲染器
//     const renderer2=new THREE.WebGL1Renderer();
//     renderer2.setSize(100,100)
//     //挂载dom
//     document.getElementsByClassName('small2')[j].appendChild(renderer2.domElement);
//     //把相机和场景渲染进去
//     renderer2.render(scene2,camera2)

//     //添加材料
//     const materia2=new THREE.MeshBasicMaterial({
//       color:0x002299
//     })
//     //几何体设置位置
//     geometry2.center();
//     geometry2.scale(0.5,0.5,0.5)
//     //创建网格
//     const cube2 = new THREE.Mesh(geometry2,materia2)
//     //将网格放入渲染器当中
//     scene2.add(cube2)
//     //重新渲染
//     renderer2.render(scene2,camera2)
//     //自己旋转
//     // function animate(){
//     //   cube2.rotation.x+=0.02;
//     //   cube2.rotation.y+=0.02;
//     //   requestAnimationFrame(animate)
//     //   renderer2.render(scene2,camera2)
//     // }
//     // animate()
//   })
// }