import { OrbitControls } from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
import {ArcCurve, Line, LineBasicMaterial, Math, PerspectiveCamera, Scene, WebGLRenderer } from "../node_modules/three/build/three.module.js";

const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,1000)
camera.position.set(0,6,6)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer()
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls=new OrbitControls(camera,renderer.domElement)

const geometry=new Geometry()
const arc=new ArcCurve(0,0,100,0,2*Math.PI)
const points=arc.getPoints(50)
geometry.setFromPoints(points)
const material=new LineBasicMaterial({
  color:0x000000
})
const line=new Line(geometry,material)
scene.add(line)