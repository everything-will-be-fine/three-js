import {
  BoxGeometry,
  Color,
  DirectionalLight,
  Fog,
  FogExp2,
  Mesh,
  MeshPhongMaterial,
  PerspectiveCamera,
  Scene,
  WebGLRenderer
} from '../node_modules/three/build/three.module.js';
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js';

//-------------基础模板--------------------
const scene = new Scene()
const camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.set(0, 0, 10)
camera.lookAt(0, 0, 0)
const renderer = new WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)
const controls = new OrbitControls(camera, renderer.domElement)

//--------------不停刷新-----------------------
function render(time) {
  time *= 0.001
  renderer.render(scene, camera)
  cube.rotation.x = time
  cube.rotation.y = time
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-------创建几何体---------------
const geometry = new BoxGeometry(3, 3, 3)
const material = new MeshPhongMaterial({
  color: 0x8844aa
})
//material.fog=false
const cube = new Mesh(geometry, material)
scene.add(cube)

//------------------灯光-----------------------
{
  const color=0xffffff
  const intensity=1
  const light=new DirectionalLight(color,intensity)
  light.position.set(-1,2,4)
  scene.add(light)
}

//----------.Fog() 线性雾-----------------
// {
//   const near = 1
//   const far = 11
//   const color = 'lightblue'
//   scene.fog = new Fog(color, near, far)
//   scene.background = new Color(color)
// }
//-----------.FogExp2() 指数雾（更真实一点）-----------
{
  const color='lightblue'
  const density=0.1
  scene.fog=new FogExp2(color,density)
  scene.background=new Color(color)
}