//import * as from '../js-dev/build/module.js'
import {
  Scene,
  PerspectiveCamera,
  Vector3,
  WebGL1Renderer,
  MeshBasicMaterial,
  Mesh,
  Vector2,
  AmbientLight,
  PlaneGeometry,
  MeshLambertMaterial,
  SpotLight
} from '../node_modules/three/build/three.module.js'
import {
  StereoEffect
} from '../node_modules/three/examples/jsm/effects/StereoEffect.js'
import {
  STLLoader
} from '../node_modules/three/examples/jsm/loaders/STLLoader.js'
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'


//创造场景
const scene = new Scene();
//创造相机
const camera = new PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 100000)
//相机方向
camera.position.set(0, 0, 230);
camera.lookAt(scene.position)
//创建渲染器
const renderer = new WebGL1Renderer();
//设置大小,颜色
renderer.setClearColor(0xeeeeee, 0.5)
renderer.setSize(window.innerWidth, window.innerHeight)
//挂载dom
document.body.appendChild(renderer.domElement)
//载入场景和摄像机
renderer.render(scene, camera)
//创建一个实例化对象
const VrRender = new StereoEffect(renderer)
//设置渲染器恢复默认状态
const size = renderer.getSize();
renderer.setViewport(0, 0, size.width, size.height);
VrRender.render(scene,camera)
// //添加三维轴
// const axes = new AxesHelper(100)
// scene.add(axes)

//动脉血管
const loader = new STLLoader();
loader.load('./pic/动脉血管  .stl', geometry => {
  //设置位置
  geometry.center();
  //设置大小
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0xff0000
  })
  //创建网格
  const cube = new Mesh(geometry, materia)
  //网格位置
  cube.rotation.x = 30
  scene.add(cube)
  renderer.render(scene, camera)
})
//下腔静脉
loader.load('./pic/下腔静脉.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0x0000ff
  })
  //创建网格
  const cube = new Mesh(geometry, materia)
  //网格位置
  cube.rotation.x = 30;
  cube.translateZ(-3) //y
  cube.translateY(-1) //z
  cube.translateX(-12) //x
  //cube.translateY
  scene.add(cube)
  //刷新
  renderer.render(scene, camera)
})

//门静脉血管
loader.load('./pic/门静脉血管 .stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x666CA
  })
  //创建网格
  const cube = new Mesh(geometry, materia)
  //设置位置
  cube.rotation.x = 30
  cube.translateZ(-10)
  scene.add(cube)
})

//肝静脉血管
loader.load('./pic/肝静脉血管 .stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x0000ff
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(-30)
  cube.translateY(3)
  cube.translateZ(3)
  cube.rotateY(0.7)
  cube.rotateX(-1)
  scene.add(cube)
})

//脾脏
loader.load('./pic/脾脏.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x7373AB
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(32)
  cube.translateY(18)
  cube.translateZ(-18)
  scene.add(cube)
})

//胰管
loader.load('./pic/胰管 .stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x00FA00
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(4)
  cube.translateY(-12)
  cube.translateZ(8)
  cube.rotateX(-1.5)
  scene.add(cube)
})
//胰脏
loader.load('./pic/胰脏.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0xAA8663,
    opacity: 0.9,
    transparent: true

  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(10)
  cube.translateY(-6)
  cube.translateZ(3)
  cube.rotateX(-1.5)
  scene.add(cube)
})
//胆总管
loader.load('./pic/胆总管.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x006C00,
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(-13)
  cube.translateY(-2)
  cube.translateZ(9)
  cube.rotateX(-1.5)
  scene.add(cube)
})

//胆囊
loader.load('./pic/胆囊.stl', geometry => {
  geometry.center()
  geometry.scale(0.6, 0.6, 0.6)
  const materia = new MeshBasicMaterial({
    color: 0x006C00,
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(-14)
  cube.translateY(-8)
  cube.translateZ(14)
  cube.rotateX(-1)
  scene.add(cube)
})
//部分肠管
loader.load('./pic/部分肠管.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x602F00
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(50)
  cube.translateY(10)
  cube.translateZ(-10)
  cube.rotateX(-1.7)
  cube.rotateY(-0.3)
  scene.add(cube)
})
//部分胃
loader.load('./pic/部分胃.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x3C003C
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(21)
  cube.translateY(10)
  cube.translateZ(10)
  cube.rotateX(-1.3)
  scene.add(cube)
})
//扩张胆管
loader.load('./pic/扩张胆管 .stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x006C00,
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(-14)
  cube.translateY(10)
  cube.translateZ(10)
  cube.rotateX(-1)
  scene.add(cube)
})
//淋巴结
loader.load('./pic/淋巴结.stl', geometry => {
  //设置位置
  geometry.center();
  //设置大小
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0xECEC00
  })
  //创建网格
  const cube = new Mesh(geometry, materia)
  //网格位置
  cube.translateZ(5);
  cube.rotateX(1.5)
  cube.rotateY(3)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝内胆管结石可能
loader.load('./pic/肝内胆管结石可能.stl', geometry => {
  geometry.center();
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0xFF8C00
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(1);
  cube.translateX(-20);
  cube.translateY(5)
  cube.rotateX(-2)
  scene.add(cube)
  renderer.render(scene, camera)
})
//囊肿
loader.load('./pic/囊肿 .stl', geometry => {
  geometry.center();
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0xFFB6C1
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(16.5);
  cube.translateX(-43);
  cube.translateY(3)
  cube.rotateY(1.3)
  cube.rotateX(-0.9)
  scene.add(cube)
  renderer.render(scene, camera)
})
//占位
loader.load('./pic/占位.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0xFE7F00,
  })
  const cube = new Mesh(geometry, materia)
  cube.translateX(25)
  cube.translateY(15)
  cube.translateZ(20)
  cube.rotateX(1.7)
  cube.rotateY(-1)
  scene.add(cube)
})
//肝脏（VI段）.stl
loader.load('./pic/肝脏（VI段）.stl', geometry => {
  geometry.center();
  geometry.scale(0.52, 0.52, 0.52)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0x1C6E97,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(6);
  cube.translateX(-45);
  cube.translateY(-10)
  cube.rotateY(0.5)
  cube.rotateX(-1.3)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（VII段）.stl
loader.load('./pic/肝脏（VII段）.stl', geometry => {
  geometry.center();
  geometry.scale(0.52, 0.52, 0.52)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0x6F4924,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(8);
  cube.translateX(-40);
  cube.translateY(15)
  cube.rotateY(0.5)
  cube.rotateX(-1.3)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（右后叶）.stl
loader.load('./pic/肝脏（右后叶）.stl', geometry => {
  geometry.center();
  geometry.scale(0.56, 0.58, 0.58)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0x7DB99E,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(6.9);
  cube.translateX(-40);
  cube.translateY(1)
  cube.rotateY(1521)
  cube.rotateX(-1.3)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（V段）.stl
loader.load('./pic/肝脏（V段）.stl', geometry => {
  geometry.center();
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0x00CEB0,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(30);
  cube.translateX(-31);
  cube.translateY(-14)
  cube.rotateY(1521)
  cube.rotateX(-1.3)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（VII段）.stl
loader.load('./pic/肝脏（VII段）.stl', geometry => {
  geometry.center();
  geometry.scale(0.4, 0.4, 0.4)
  //设置材质
  const materia = new MeshBasicMaterial({
    color: 0x1F766C,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(32);
  cube.translateX(-33);
  cube.translateY(15)
  cube.rotateY(0.1)
  cube.rotateX(0.2)
  cube.rotateZ(0.2)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（右前叶）.stl
loader.load('./pic/肝脏（右前叶）.stl', geometry => {
  geometry.center()
  geometry.scale(0.5, 0.5, 0.58)
  const materia = new MeshBasicMaterial({
    color: 0xA9958B,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(29);
  cube.translateX(-30);
  cube.translateY(-2)
  cube.rotateY(0.5)
  cube.rotateX(-1.35)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（右半肝）.stl
loader.load('./pic/肝脏（右半肝）.stl', geometry => {
  geometry.center()
  geometry.scale(0.6, 0.6, 0.6)
  const materia = new MeshBasicMaterial({
    color: 0x7DB99E,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(18);
  cube.translateX(-31.5);
  cube.translateY(0)
  cube.rotateY(0.5)
  cube.rotateX(-1.35)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（IV段）.stl
loader.load('./pic/肝脏（IV段）.stl', geometry => {
  geometry.center()
  geometry.scale(0.3, 0.3, 0.3)
  const materia = new MeshBasicMaterial({
    color: 0x0000FF,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(35);
  cube.translateX(-1.5);
  cube.translateY(9)
  cube.rotateY(0.5)
  cube.rotateX(-1.1)
  cube.rotateZ(0.2)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（III段）.stl
loader.load('./pic/肝脏（III段）.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0xBAAA56,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(22);
  cube.translateX(20);
  cube.translateY(1.7)
  cube.rotateY(0.5)
  cube.rotateX(4.1)
  // cube.rotateZ(0.2)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（II段）.stl
loader.load('./pic/肝脏（II段）.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0xEEA059,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(20);
  cube.translateX(25);
  cube.translateY(15)
  cube.rotateY(0.2)
  cube.rotateX(-0.5)
  cube.rotateZ(0.2)
  scene.add(cube)
  renderer.render(scene, camera)
  //  function render(){
  //   renderer.render(scene,camera)
  //   cube.rotation.x+=0.02
  //   requestAnimationFrame(render)
  // }
  // render()
})
//肝脏（左外叶）.stl
loader.load('./pic/肝脏（左外叶）.stl', geometry => {
  geometry.center()
  geometry.scale(0.33, 0.53, 0.57)
  const materia = new MeshBasicMaterial({
    color: 0x54747B,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(24);
  cube.translateX(28);
  cube.translateY(6)
  cube.rotateY(-0.2)
  cube.rotateX(-1.2)
  // cube.rotateZ(0.2)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（左半肝）.stl
loader.load('./pic/肝脏（左半肝）.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.58, 0.6)
  const materia = new MeshBasicMaterial({
    color: 0xB59A98,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(32);
  cube.translateX(19);
  cube.translateY(-6)
  cube.rotateY(0.2)
  cube.rotateX(-1.5)
  // cube.rotateZ(0.2)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（去尾状叶）.stl
loader.load('./pic/肝脏（去尾状叶）.stl', geometry => {
  geometry.center()
  geometry.scale(0.55, 0.6, 0.7)
  const materia = new MeshBasicMaterial({
    color: 0x917DA1,
    opacity: 0.7,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(18);
  cube.translateX(-5);
  cube.translateY(0)
  cube.rotateY(0.2)
  cube.rotateX(-1.35)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏（尾状叶）.stl
loader.load('./pic/肝脏（尾状叶）.stl', geometry => {
  geometry.center()
  geometry.scale(0.4, 0.4, 0.4)
  const materia = new MeshBasicMaterial({
    color: 0x00FFFF,
    opacity: 0.4,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(5);
  cube.translateX(-10);
  cube.translateY(20)
  // cube.rotateY(0.2)
  cube.rotateX(-1.35)
  scene.add(cube)
  renderer.render(scene, camera)
})
//肝脏.stl
loader.load('./pic/肝脏.stl', geometry => {
  geometry.center()
  geometry.scale(0.65, 0.65, 0.86)
  const materia = new MeshBasicMaterial({
    color: 0x800000,
    opacity: 0.5,
    transparent: true
  })
  const cube = new Mesh(geometry, materia)
  cube.translateZ(17);
  cube.translateX(-1);
  cube.translateY(2)
  cube.rotateY(0.1)
  cube.rotateX(-1.35)
  scene.add(cube)
  renderer.render(scene, camera)
})
//双屏渲染
VrRender.render(scene, camera)

//控制鼠标
function render() {
  VrRender.render(scene, camera)
  requestAnimationFrame(render)
}
render()
const controls = new OrbitControls(camera, renderer.domElement)


