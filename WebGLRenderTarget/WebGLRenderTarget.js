import {
  MeshBasicMaterial,
  WebGLRenderTarget,
  Color,
  Fog,
  BoxGeometry,
  MeshPhongMaterial,
  Mesh,
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  DirectionalLight
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from "../node_modules/three/examples/jsm/controls/OrbitControls.js";

//--------------基础模板---------------

const scene = new Scene()
const camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.set(0, 0, 10)
camera.lookAt(0, 0, 0)
const renderer = new WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)
renderer.render(scene, camera)

const controls = new OrbitControls(camera, renderer.domElement)


//------------光源------------
{
  const color = 0xffffff
  const intensity = 1
  const light = new DirectionalLight(color, intensity)
  light.position.set(-1, 2, 4)
  scene.add(light)
}

//---------------几何体-----------------
const geometry = new BoxGeometry(4, 4, 4)

//-------------创建fog-----------------
// {
//   const color = 'lightblue'
//   scene.fog = new Fog(color, 1, 11)
//   scene.background = new Color(color)
// }


//------------------创建新的渲染器--------------
//使用渲染器就需要一个Camera（相机） 和一个 Scene（场景）。
//这里使用的相机比例（rtAspect）是几何体面的比例，不是画布的比例。
const rtwidth = 512
const rtheight = 512
const renderTarget = new WebGLRenderTarget(rtwidth, rtheight)


const rtcamera = new PerspectiveCamera(75, rtwidth / rtheight, 0.1, 5)
rtcamera.position.z = 2
const rtScene = new Scene()
rtScene.background = new Color('white')

//有了场景，需要展示的纹理就和普通的绘制是一样的。添加几何体。
const rtBox = 1
const rtGeometry = new BoxGeometry(rtBox, rtBox, rtBox)
const rtMaterial = new MeshBasicMaterial({
  color: 0x44aa88
})
const rtCube = new Mesh(rtGeometry, rtMaterial)
rtScene.add(rtCube)

//在几何体中使用缓存画面-----更改几何体的材料------------------------
const material = new MeshPhongMaterial({
  map: renderTarget.texture
})
const cube = new Mesh(geometry, material)
scene.add(cube)

//渲染时，我们首先渲染目标的场景(rtScene)，在渲染要在画布上展示的场景。
//-------------渲染--------------
function render(time) {
  renderer.render(scene, camera)
  time *= 0.001
  cube.rotation.y = time
  cube.rotation.x = time
  requestAnimationFrame(render)
  // 重要的一步
  renderer.setRenderTarget(renderTarget)
  renderer.render(rtScene, rtcamera)
  renderer.setRenderTarget(null)
  // // 设置纹理动画
  // rtCube.rotation.y = time
  // rtCube.rotation.x = time
}
requestAnimationFrame(render)