import {
  DoubleSide,
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Shape,
  ShapeGeometry,
  MeshBasicMaterial,
  Mesh,
  DirectionalLight,
  ExtrudeGeometry,
  MeshPhongMaterial,
  Path
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'
//-----------基础模板-----------------------------------
const scene = new Scene()
const camera = new PerspectiveCamera(40,2, 0.1, 1000)
camera.position.set(0, 10, 50)
camera.lookAt(0, 0, 0)
const renderer = new WebGLRenderer({
  antialias: true
})
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)
const controls = new OrbitControls(camera, renderer.domElement)

//--------------不停刷新-----------------------------------
function render(time) {
  time *= 0.05
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//---------------------绘制心性路径-----------------
const heartShape = new Shape()
heartShape.moveTo(0, 1.5) //将绘图点移动到指定的 x、y 坐标处
heartShape.bezierCurveTo(2, 3.5, 4, 1.5, 2, -0.5) //创建一条到x、y 坐标的贝塞尔曲线
heartShape.lineTo(0, -2.5) //从当前位置创建一条到 x、y 坐标的线
heartShape.lineTo(-2, -0.5)
heartShape.bezierCurveTo(-4, 1.5, -2, 3.5, 0, 1.5)
//--------------绘制心形平面-------------------------
// const geometry=new ShapeGeometry(heartShape)
// const material=new MeshBasicMaterial({color:0x00ff00,side:DoubleSide})
// const mesh=new Mesh(geometry,material)
// scene.add(mesh)

//-----------拉伸三维图形--------------------
{
  const color = 0xffffff
  const intensity = 1
  const light = new DirectionalLight(color, intensity)
  light.position.set(-1, 10, 4)
  scene.add(light)
}
{
  const color = 0xffffff
  const intensity = 1
  const light = new DirectionalLight(color, intensity)
  light.position.set(-1, -10, -4)
  scene.add(light)
}

const extrudeSettings = {
  steps: 0.1,//用于沿着挤出样条的深度细分的点的数量，默认1
  depth:0.5//挤出形状的深度，默认100
}
const geometry2 = new ExtrudeGeometry(heartShape, extrudeSettings)
const material2 = new MeshPhongMaterial({
  color: 0x00ff00,
  side: DoubleSide
})
const mesh2 = new Mesh(geometry2, material2)
scene.add(mesh2)

//挖洞
const shape_c=new Path()
shape_c.moveTo(-1,1)
shape_c.lineTo(-1,-1)
shape_c.lineTo(1,-1)
shape_c.lineTo(1,1)
shape_c.lineTo(-1,1)
heartShape.holes.push(shape_c)