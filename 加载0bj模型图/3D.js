import {
  DoubleSide,
  DirectionalLight,
  HemisphereLight,
  Color,
  WebGLRenderer,
  Scene,
  PerspectiveCamera
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
import {
  OBJLoader
} from '../node_modules/three/examples/jsm/loaders/OBJLoader.js'
import{MTLLoader}from'../node_modules/three/examples/jsm/loaders/MTLLoader.js'
//------------模板创建-------------
const scene = new Scene()
scene.background = new Color('black')
const camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.set(0, 0, 10)
camera.lookAt(0, 0, 0)
const renderer = new WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)
renderer.render(scene, camera)

const controls=new OrbitControls(camera,renderer.domElement)

//---------------刷新-------------------
function render(time) {
  time *= 0.05
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-------------光源-------------------
{ //半球光
  const skyColor = 0xb1eff
  const groundColor = 0xffffff
  const intensity = 1
  const light = new HemisphereLight(skyColor, groundColor, intensity)
  scene.add(light)
} 
{ //方向光
  const color = 0xffffff
  const intensity = 1
  const light = new DirectionalLight(color, intensity)
  light.position.set(0, 10, 0)
  light.target.position.set(-5, 0, 0)
  scene.add(light)
  scene.add(light.target)
}
//---------引入OBJLoader解析文件------------
//------------引入配套的mtl文件-------------------
const mtlLoader = new MTLLoader()
mtlLoader.load('./images/47-obj/obj/Handgun_obj.mtl', (mtl) => {
  mtl.preload()
  const objloader = new OBJLoader()
  objloader.setMaterials(mtl)
  //加载模型
  objloader.load('./images/47-obj/obj/Handgun_obj.obj', (root) => {
    scene.add(root)
  })
  //通过MTLLoader对象的materials属性获取材质
  for(const material of Object.values(mtl.setMaterials)){
    console.log('material',material)
    //设置材质双面
    material.side=new DoubleSide()
  }
})
