import {Shape, AxisHelper, Color, DoubleSide, ExtrudeGeometry, Fog, HemisphereLight, Mesh, MeshBasicMaterial, MeshPhongMaterial, NearestFilter, PerspectiveCamera, PlaneGeometry, RepeatWrapping, Scene, TextureLoader, WebGLRenderer, Path, BoxGeometry, MeshLambertMaterial, Group } from "../node_modules/three/build/three.module.js";
import{OrbitControls}from'../node_modules/three/examples/jsm/controls/OrbitControls.js'
//--------------基本模板----------------------------
const scene=new Scene()
const camera=new PerspectiveCamera(80,2,0.1,1000)
camera.position.set(300,200,0)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer()
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)
const controls=new OrbitControls(camera,renderer.domElement) 

//------------不停刷新--------------------
function render(time){
  time*=0.05
  renderer.render(scene,camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-------绘制地面--辅助线-----------
const axes=new AxisHelper(700)
scene.add(axes)
//增加光源，增强场景立体感
{
  const skyColor=0xffffff
  const groundColor=0x000000
  const intensity=1
  const light=new HemisphereLight(skyColor,groundColor,intensity)
  scene.add(light)
}
//加载地板和地面纹理
{
  const loader=new TextureLoader()
  const texture=loader.load('./images/ground.webp')
  texture.wrapS=RepeatWrapping
  texture.wrapT=RepeatWrapping
  texture.magFilter=NearestFilter
  //纹理重复
  texture.repeat.set(100,100)
  const planeGeo=new PlaneGeometry(10000,10000)
  const planMat=new MeshPhongMaterial({
    map:texture,
    side:DoubleSide
  })
  const mesh=new Mesh(planeGeo,planMat)
  mesh.rotation.x=Math.PI*-0.5
  scene.add(mesh)
}
//添加背景
scene.background=new Color(0x87ceeb)
scene.fog=new Fog(0x87ceeb,200,10000)

{
  //绘制房子
  const loader=new TextureLoader()
  const texture=loader.load('./images/ground2.webp')
  texture.wrapS=RepeatWrapping
  texture.wrapT=RepeatWrapping
  texture.magFilter=NearestFilter
  texture.repeat.set(2,2)
  const planeGeo=new PlaneGeometry(300,300)
  const planMat=new MeshPhongMaterial({
    map:texture,
    side:DoubleSide
  })
  const mesh=new Mesh(planeGeo,planMat)
  mesh.rotation.x=Math.PI*-0.5
  mesh.position.y=1
  scene.add(mesh)
}
//绘制后墙和左右墙（可以使用立方体，也可以使用shape）
const extrudeSettings={
  amount:8,
  bevelSegments:2,
  steps:2,
  bevelSize:1,
  bevelThickness:1
}
{
  //绘制左右墙
  function wallAdd(){
    const shape=new Shape()//用shape绘制二维形状
    shape.moveTo(-150,0)
    shape.lineTo(150,0)
    shape.lineTo(150,150)
    shape.lineTo(0,200)
    shape.lineTo(-150,150)
    const extrudeGeometry=new ExtrudeGeometry(shape,extrudeSettings)
    var material=new MeshBasicMaterial({
      color:0xe5d890
    })
    const sideWall=new Mesh(extrudeGeometry,material)
    sideWall.position.y=1
    return sideWall
  }
  const sideWall=wallAdd()
  const sideWall2=wallAdd()
  sideWall.position.z=-150
  sideWall2.position.z=150
  scene.add(sideWall)
  scene.add(sideWall2)
}
{
  //绘制后墙
  const shape=new Shape()
  shape.moveTo(-150,0)
  shape.lineTo(150, 0)
  shape.lineTo(150, 150)
  shape.lineTo(-150, 150)
  const extrudeGeometry=new ExtrudeGeometry(shape,extrudeSettings)
  const material=new MeshBasicMaterial({color:0xe5d890})
  const backWall=new Mesh(extrudeGeometry,material)
  backWall.position.x=-150
  backWall.position.y=1
  backWall.rotation.y=Math.PI*0.5
  scene.add(backWall)
}
{
  //前墙
  const shape=new Shape()
  shape.moveTo(-150, 0)
  shape.lineTo(150, 0)
  shape.lineTo(150, 150)
  shape.lineTo(-150, 150)
  //绘制门和窗户
  const shape_a=new Path()
  shape_a.moveTo(30, 30)
  shape_a.lineTo(80, 30)
  shape_a.lineTo(80, 80)
  shape_a.lineTo(30, 80)
  shape_a.lineTo(30, 30)
  shape.holes.push(shape_a)

  const shape_b = new Path()
  shape_b.moveTo(-20, 0)
  shape_b.lineTo(-20, 100)
  shape_b.lineTo(-80, 100)
  shape_b.lineTo(-80, 0)
  shape_b.lineTo(-20, 0)
  shape.holes.push(shape_b)
  const extrudeGeometry=new ExtrudeGeometry(shape,extrudeSettings)
  const material=new MeshBasicMaterial({color:'grey'})
  const backWall=new Mesh(extrudeGeometry,material)
  backWall.position.x=143
  backWall.position.z=4
  backWall.position.y=1
  backWall.rotation.y=Math.PI*0.5
  scene.add(backWall)
}
{
  //房顶
  function roof(){
    const roofGeometry=new BoxGeometry(200,320,10)
    const loader=new TextureLoader()
    const roofTexture=loader.load('./images/roof.webp')
    roofTexture.wrapS=roofTexture.wrapT=RepeatWrapping
    roofTexture.repeat.set(2,2)
    const textureMaterial=new MeshBasicMaterial({
      map:roofTexture
    })
    const materials=[textureMaterial,textureMaterial,textureMaterial,textureMaterial,textureMaterial,textureMaterial]
    const roof=new Mesh(roofGeometry,materials)
    return roof
  }
  const roof1=roof()
  roof1.rotation.x=Math.PI/2
  roof1.rotation.y=(-Math.PI/4)*0.4
  roof1.position.y=170
  roof1.position.x=90

  const roof2=roof()
  roof2.rotation.x=Math.PI/2
  roof2.rotation.y=(Math.PI/4)*0.4
  roof2.position.y=170
  roof2.position.x=-90
  scene.add(roof1)
  scene.add(roof2)
}
//添加门和门框
{
  //门框
  const shape=new Shape()
  shape.moveTo(-20, 0)
  shape.lineTo(-20, 100)
  shape.lineTo(-80, 100)
  shape.lineTo(-80, 0)
  shape.lineTo(-20, 0)
  const shape_c = new Path()
  shape_c.moveTo(-25, 5)
  shape_c.lineTo(-25, 95)
  shape_c.lineTo(-75, 95)
  shape_c.lineTo(-75, 5)
  shape_c.lineTo(-25, 5)
  shape.holes.push(shape_c)
  const extrudeGeometry=new ExtrudeGeometry(shape,extrudeSettings)
  const material=new MeshBasicMaterial({color:'red'})
  const frame=new Mesh(extrudeGeometry,material)

  //门
  const doorGeometry=new BoxGeometry(50,90,4)
  const doorTexture=new TextureLoader().load('./images/door.webp')
  const doorMaterial=new MeshLambertMaterial({map:doorTexture})
  const door=new Mesh(doorGeometry,doorMaterial)
  door.position.set(-50,50,5)
  const group=new Group()
  group.add(frame)
  group.add(door)
  group.position.x=143
  group.position.y=1
  group.position.z=5
  group.rotation.y=Math.PI/2
  scene.add(group)
}