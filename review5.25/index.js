import {
  Vector2,
  SpotLight,
  AmbientLight,
  PlaneGeometry,
  AxesHelper,
  BoxGeometry,
  Mesh,
  MeshLambertMaterial,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  CameraHelper
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js';
//three.js组件库
import { GUI } from 'https://threejsfundamentals.org/threejs/../3rdparty/dat.gui.module.js'
//创建场景
const scene = new Scene();

//创建相机
const camera = new PerspectiveCamera(65, window.innerHeight / window.innerWidth, 0.1, 1000)

//设置摄像机位置
camera.position.set(200, 100, 200)
//把摄像机的方向对准场景的中心点
camera.lookAt(scene.position)

//创建渲染器
const renderer = new WebGLRenderer()
//设置渲染器颜色和大小
//renderer.setClearColor(0xeeeeee, 0.5)
renderer.setSize(window.innerWidth, window.innerHeight)
//增加渲染器阴影映射功能
renderer.shadowMap.enabled = true;
//挂载dom
document.body.appendChild(renderer.domElement)

//将场景和摄像机放入渲染器当中
renderer.render(scene, camera)
// //添加相机辅助线
// const cameraHelper=new CameraHelper(camera)
// scene.add(cameraHelper)
// cameraHelper.update()
// renderer.render(scene,camera)

//添加三维轴（调整线的长短）
const axes = new AxesHelper(50);
//放入场景当中
scene.add(axes)

//创建几何体
const geometry = new BoxGeometry(10, 10, 10)
//设置位置
geometry.center()
//设置材质(因为要设置阴影，所以将材质换成可以反光的)
const materia = new MeshLambertMaterial({
  color: 0x002299
})

//创建网格（geometry，materia的结合体）
const cube = new Mesh(geometry, materia)
//cube投影功能打开
cube.castShadow = true;
//网格
cube.rotation.x = 0.8;
cube.rotation.y = 0.9;
//将网格放入场景当中
scene.add(cube)

//创建一个平面几何体
const planeGeometry = new PlaneGeometry(100, 100)
//设置场面材质(因为要观察反射光，所以不能用基础材质)
const planeMaterial = new MeshLambertMaterial({
  color: 0xcccccc
})
//合成
const plane = new Mesh(planeGeometry, planeMaterial)
//设置平面可接受阴影
plane.receiveShadow = true;
//设置平面的位置
plane.rotation.x = -0.5 * Math.PI;
plane.position.set(15, -10, 0)
//添加进入场景
scene.add(plane)
renderer.render(scene, camera)
//采用MeshLambertMaterial，需要有光源才行（环境光，没有确定的方向）
const ambientLight = new AmbientLight(0xaaaaaa);
scene.add(ambientLight)
renderer.render(scene, camera)

//设置聚光灯
const spotLight = new SpotLight(0x00FA9A)
spotLight.position.set(-60, 40, -65)
//开启阴影效果
spotLight.castShadow = true;
//设置阴影效果
spotLight.shadow.mapSize = new Vector2(1024, 1024)
spotLight.shadow.camera.far = 130;
spotLight.shadow.camera.near = 40;
scene.add(spotLight)

//控制鼠标
function render() {
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
render()
const controls = new OrbitControls(camera, renderer.domElement)
// //自己动起来
// function animate(){
//   renderer.render(scene,camera);
//   cube.rotation.x+=0.02;
//   cube.rotation.y+=0.02;
//   requestAnimationFrame(animate)
// }
// animate()
