
import {Vector3,CatmullRomCurve3,DirectionalLight,NearestFilter, DoubleSide, Mesh, MeshLambertMaterial, PerspectiveCamera, PlaneGeometry, RepeatWrapping, Scene, TextureLoader, WebGLRenderer, MeshPhongMaterial, MeshBasicMaterial, TubeBufferGeometry, BackSide } from '../node_modules/three/build/three.module.js';
import{OrbitControls} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'
const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,1000)
camera.position.set(0,200,600)
camera.lookAt(0,0,0)

const renderer=new WebGLRenderer({
  alpha:true
})
document.body.appendChild(renderer.domElement)
renderer.setSize(window.innerWidth,window.innerHeight)
//灯光
{
  const color=0xffffff
  const intensity=1
  const light=new DirectionalLight(color,intensity)
  light.position.set(0,6,4)
  scene.add(light)
}

const controls=new OrbitControls(camera,renderer.domElement)
const plane=new PlaneGeometry(400,400)
const texture=new TextureLoader().load('./images/floor2.webp')
texture.wrapS=texture.wrapT=RepeatWrapping
texture.magFilter=NearestFilter
texture.repeat.set(5,5)
const material=new MeshPhongMaterial({
  map:texture,
  side:DoubleSide
})
const mesh=new Mesh(plane,material)
mesh.rotation.x=Math.PI*-0.5
scene.add(mesh)


 //创造长条平面图
 const long=new PlaneGeometry(200,30)
 //加载纹理
 const arrowLineTexture=new TextureLoader().load('./images/arrow.webp')
 arrowLineTexture.wrapS=arrowLineTexture.wrapT=RepeatWrapping
 arrowLineTexture.repeat.set(10,1)
 arrowLineTexture.needsUpdata=true
 //贴图
 const material2=new MeshBasicMaterial({
  map:arrowLineTexture,
  side:DoubleSide
 })
 const mesh2=new Mesh(long,material2)
 mesh2.position.set(20,30,20)
 scene.add(mesh2)

 //-------------创建轨道---------------
// 创建线条路径
let curve = new CatmullRomCurve3([
  new Vector3(0, 0, 10),
  new Vector3(10, 0, 10),
  new Vector3(10, 0, 0),
  new Vector3(20, 0, -10)
]);

const tubeGeometry=new TubeBufferGeometry(curve,200,0.1)
//加载纹理
const texture2=new TextureLoader().load('./images/arrow2.webp')
texture2.wrapS=texture2.wrapT=RepeatWrapping
texture2.repeat.set(20,1)
texture2.needsUpdata=true
//创建纹理
const materialCurve=new MeshBasicMaterial({
  map:texture2,
  side:BackSide,
  transparent:true
})
const mesh3=new Mesh(tubeGeometry,materialCurve)
mesh3.position.set(50,100,20)
mesh3.scale.set(5,100,20)
scene.add(mesh3)

//更新纹理偏移
function render(){
  arrowLineTexture.offset.x-=0.03
  texture2.offset.x-=0.05
  renderer.render(scene,camera)
  requestAnimationFrame(render)
 }
 requestAnimationFrame(render)
