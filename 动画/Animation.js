import {Mesh,AxesHelper, Fog, WebGLRenderer, Color, HemisphereLight, PerspectiveCamera, Scene, TextureLoader, RepeatWrapping, NearestFilter,MeshPhongMaterial, DoubleSide, PlaneGeometry, BoxGeometry, DirectionalLight, Vector3} from "../node_modules/three/build/three.module.js"
import{OrbitControls}from'../node_modules/three/examples/jsm/controls/OrbitControls.js'
import{FBXLoader}from'../node_modules/three/examples/jsm/loaders/FBXLoader.js'
//-------------基本模板---------------------
const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,1000)
camera.position.set(-400,400,0)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer({
  antialias:true
})
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)

//const controls=new OrbitControls(camera,renderer.domElement)

//-------------不停刷新---------------
function render(time){
  time*=0.05
  renderer.render(scene,camera)
  if (meshHY) {
    onCodeMove(meshHY)
  }
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//----------雾-----------------
scene.background=new Color(0x87ceeb)
scene.fog=new Fog(0x87ceeb,1,11)

//----------辅助-------------
const axes=new AxesHelper(700)
scene.add(axes)

//------------灯光--------------
{
  const color=0xffffff//天空白色
  const groundcolor=0x000000//地面黑色
  const intensity=1
  const light=new HemisphereLight(color,groundcolor,intensity)
  scene.add(light)
}
{
  //创建地面
  const loader=new TextureLoader()
  const texture=loader.load('./fbx/ground.webp')
  texture.wrapS=RepeatWrapping
  texture.wrapT=RepeatWrapping//重复
  texture.magFilter=NearestFilter//临近过滤：放大会导致方块化，缩小会丢失细节
  texture.repeat.set(100,100)//每个重复的大小

  const planeGeo=new PlaneGeometry(10000,10000)
  const planeMat=new MeshPhongMaterial({
    map:texture,
    side:DoubleSide
  })
  const mesh=new Mesh(planeGeo,planeMat)
  mesh.rotation.x=Math.PI*-0.5
  //地面开启接受阴影
  mesh.receiveShadow=true
  scene.add(mesh)
}
// //加载fbx模型
// var loader2=new FBXLoader()
// loader2.load('./fbx/01/001 - Bulbasaur/BR_Bulbasaur-Shiny01.fbx',function(mesh){
//   console.log(' AnimationMixer 动画混合器（二十四）.html ~ line 73 ~ mesh', mesh)
//   mesh.position.y=110
//   scene.add(mesh)
// })
//创建方块，实现方块移动
const Geometry=new BoxGeometry(20,20,20)
const material=new MeshPhongMaterial({
  color:0x668866
})
const cube=new Mesh(Geometry,material)
cube.position.set(0,20,0)
scene.add(cube)

//----------开启阴影------------------
renderer.shadowMap.enabled=true
let dLight=null
{
  const light=new DirectionalLight(0xaaaaaa)
  light.position.set(0,200,200)
  light.lookAt(new Vector3())
  light.castShadow=true
  light.castShadow = true
  light.shadow.camera.top = 300
  light.shadow.camera.bottom = -300
  light.shadow.camera.left = -300
  light.shadow.camera.right = 300
   // 开启阴影投射
  light.castShadow = true
  dLight = light
  scene.add(light)
}
let meshHY=null
cube.castShadow=true
cube.receiveShadow=true
dLight.target=cube
// 设置光线焦点模型
meshHY=cube

//修改键盘监听事件，监听键盘是否按下
  let keyCodeW = false
  let keyCodeS = false
  let keyCodeA = false
  let keyCodeD = false
  let keyCodeK = false // 跳跃
  let attackCombo = true
  let clickNum = 0 // 点击次数
  document.addEventListener(
    'keydown',
    (e) => {
      var ev = e || window.event
      switch (ev.keyCode) {
        case 87:
          keyCodeW = true
          break
        case 83:
          keyCodeS = true
          break
        case 65:
          keyCodeA = true
          break
        case 68:
          keyCodeD = true
          break
        case 75:
          keyCodeK = true
          break
        default:
          break
      }
    },
    false
  )
  document.addEventListener(
    'keyup',
    (e) => {
      var ev = e || window.event
      switch (ev.keyCode) {
        case 87:
          keyCodeW = false
          break
        case 83:
          keyCodeS = false
          break
        case 65:
          keyCodeA = false
          break
        case 68:
          keyCodeD = false
          break
        case 75:
          keyCodeK = false
          break
        default:
          break
      }
    },
    false
  )

  //根据按键控制模型移动，控制模型的的朝向。同时控制方向光和相机一起跟随模型移动。
        // 控制 移动
  function onCodeMove(cube) {
          if (keyCodeW) {
            cube.position.x += 2
            camera.position.x += 2
            dLight.position.x += 2
          }
          if (keyCodeA) {
            cube.position.z -= 2
            camera.position.z -= 2
            dLight.position.z -= 2
          }
          if (keyCodeS) {
            cube.position.x -= 2
           camera.position.x -= 2
            dLight.position.x -= 2
          }
          if (keyCodeD) {
            cube.position.z += 2
            camera.position.z += 2
            dLight.position.z += 2
          }
  
          if (keyCodeW && keyCodeD) {
            cube.rotation.y = Math.PI * 0.25
          }
          if (keyCodeW && keyCodeA) {
            cube.rotation.y = Math.PI * 0.75
          }
          if (keyCodeA && keyCodeS) {
            cube.rotation.y = Math.PI * 1.25
          }
          if (keyCodeS && keyCodeD) {
            cube.rotation.y = Math.PI * 1.75
          }
          if (keyCodeK) {
            attack()
            function attack(){
              cube.position.y+=5
              setTimeout(function(){
                cube.position.y=20
              },500)
            }
          }
}
  
