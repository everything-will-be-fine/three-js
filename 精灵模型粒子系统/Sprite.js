
import { DirectionalLight,RepeatWrapping,TextureLoader,SpriteMaterial, Sprite, Scene, PerspectiveCamera, WebGLRenderer, PlaneGeometry, MeshLambertMaterial, Mesh,Color, Fog, AmbientLight, Group, DoubleSide } from'../node_modules/three/build/three.module.js';
import{OrbitControls} from'../node_modules/three/examples/jsm/controls/OrbitControls.js'
const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,10000)
camera.position.set(1300,400,200)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer()
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls=new OrbitControls(camera,renderer.domElement)
{
  const color=0xffffff
  const intensity=1
  const light=new DirectionalLight(color,intensity)
  light.position.set(-1,2,4)
  scene.add(light)
}
//改变背景色
scene.background=new Color('lightblue')
//scene.fog=new Fog('lightblue',1,11)



//---------精灵创建树林效果---------------------------
const textureTree=new TextureLoader().load('./images/tree.png');

//------------创建一个草地地面---------------
const plane=new PlaneGeometry(1000,1000)
const textureGrass=new TextureLoader().load('./images/grass.webp')
//设置纹理重复样式
textureGrass.wrapS=RepeatWrapping;
textureGrass.wrapT=RepeatWrapping;
//uv两个方向纹理重复数量
textureGrass.repeat.set(10,10)
const material=new MeshLambertMaterial({
  map:textureGrass,
  side:DoubleSide
})
const mesh=new Mesh(plane,material)
scene.add(mesh)
mesh.rotateX(-Math.PI/2)

//批量创建表示一个树的精灵模型
for(let i=0;i<50;i++){
  const spriteMaterial=new SpriteMaterial({
    map:textureTree
  })
  //创建精灵模型对象
  const sprite=new Sprite(spriteMaterial)
  //控制精灵大小
  sprite.scale.set(50,50)
  //通过javascript随机函数Math.random()使精灵模型的位置随机分布。
  var k1=Math.random() - 0.5
  var k2=Math.random() - 0.5
  //设置精灵模型位置，在xoz平面上随机分布
  sprite.position.set(1000*k1,1000*k2)
  const trees=new Group()
  trees.add(sprite)
  scene.add(trees)
  trees.rotateX(-Math.PI/2)
  trees.position.y=27
}
//精灵模型创建雨滴
const textureRain=new TextureLoader().load('./images/rain.png')
//批量创建雨滴精灵模型
for(let i=0;i<100;i++){
  const spriteRainMaterial=new SpriteMaterial({
    map:textureRain,//设置精灵纹理贴图
  })
  //创建精灵雨滴模型对象
  const spriteRain = new Sprite(spriteRainMaterial)
  //控制雨滴大小
  spriteRain.scale.set(50,50)
  var k3=Math.random()-0.5
  var k4=Math.random()-0.5
  var k5=Math.random()-0.5
  //设置精灵模型位置，在整个空间上随机分布
  spriteRain.position.set(800*k3,350*k4,800*k5)
  const groupRain=new Group()
  groupRain.add(spriteRain)
  scene.add(groupRain)
  groupRain.position.y=240
  function move(){
    groupRain.children.forEach(spriteRain=>{
      //雨滴的y坐标每次减1
      spriteRain.position.y-=1
      if(spriteRain.position.y<-200){
        //如果雨滴落到地面，重置y，重新下落
        spriteRain.position.y=240
      }
    })
    renderer.render(scene,camera)
    requestAnimationFrame(move)
  }
  requestAnimationFrame(move)
}
//----------不停刷新--------------
function render(){
  renderer.render(scene,camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)
