import {
  SplineCurve,
  Line,
  BufferGeometry,
  LineBasicMaterial,
  Vector2,
  Vector3,
  SphereGeometry,
  CylinderGeometry,
  Mesh,
  MeshPhongMaterial,
  PlaneGeometry,
  WebGLRenderer,
  PerspectiveCamera,
  Scene,
  DirectionalLight,
  Object3D,
  BoxGeometry
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'
//创建场景
const scene = new Scene()
//创建渲染器
const renderer = new WebGLRenderer({
  antialias: true //抗锯齿
})
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

//------------创建公用相机----------------------------------------------
function makeCamera(fov = 40) {
  const aspect = 2;
  const zNear = 0.1;
  const zFar = 1000
  return new PerspectiveCamera(fov, aspect, zNear, zFar)
}
const camera = makeCamera()
//.multiplyScalar() 矩阵的每个元素乘以参数。
camera.position.set(8, 4, 10).multiplyScalar(3)
camera.lookAt(0, 0, 0)
const controls = new OrbitControls(camera, renderer.domElement)
controls.update()

renderer.render(scene, camera)
//初始化灯光(方向光)(类似太阳光，平行光)
const light1 = new DirectionalLight(0xffffff, 1)
light1.position.set(0, 20, 0)
scene.add(light1)

const light2 = new DirectionalLight(0xffffff, 1)
light2.position.set(1, 2, 4)
scene.add(light2)

// ----------绘制物体----------------------------------------------------
//创建平面
const ground = new PlaneGeometry(50, 50)
const groundMateria = new MeshPhongMaterial({
  color: 0xcc8866
})
const groundMesh = new Mesh(ground, groundMateria)
groundMesh.rotation.x = Math.PI * -0.5 // Math.PI是Π值
scene.add(groundMesh)
renderer.render(scene, camera)

//绘制多个几何体组合一个物体时，我们移动坦克的时候是所有的几何体都要移动。这里我们就要分析这个物体，那些部位是要一起变化的。把需要一起变化的几何体都放入局部空间中（子节点的变化都是在父节点变化后基础上变化的）。
//创建坦克局部空间，几何体都放入这个局部空间。在全局空间中移动这个局部空间，就是移动整个坦克。

const tank = new Object3D()
scene.add(tank)

//创建轮胎和底盘
//底盘
const carWidth = 4
const carHeight = 1
const carLength = 8
const bodyGeometry = new BoxGeometry(carWidth, carHeight, carLength)
const bodyMaterial = new MeshPhongMaterial({
  color: 0x6688aa
})
const bodyMesh = new Mesh(bodyGeometry, bodyMaterial)
bodyMesh.position.y = 1.4
tank.add(bodyMesh)

// 车轮(圆柱体)
const wheelRadius = 1;
const wheelThickness = 0.5
const wheelSegments = 36

const wheelGeometry = new CylinderGeometry(
  wheelRadius, //圆柱体顶部圆的半径
  wheelRadius, //圆柱底部圆的半径
  wheelThickness, //高度
  wheelSegments //x轴分成多少段
)
const wheelMaterial = new MeshPhongMaterial({
  color: 0x888888
})
//根据底盘  定位轮胎位置
const wheelPosition = [
  [-carWidth / 2 - wheelThickness / 2, -carHeight / 2, carLength / 3],
  [carWidth / 2 + wheelThickness / 2, -carHeight / 2, carLength / 3],
  [-carWidth / 2 - wheelThickness / 2, -carHeight / 2, 0],
  [carWidth / 2 + wheelThickness / 2, -carHeight / 2, 0],
  [-carWidth / 2 - wheelThickness / 2, -carHeight / 2, -carLength / 3],
  [carWidth / 2 + wheelThickness / 2, -carHeight / 2, -carLength / 3]
]
const wheelMeshes = wheelPosition.map((position) => {
  const cube = new Mesh(wheelGeometry, wheelMaterial)
  cube.position.set(...position)
  cube.rotation.z = Math.PI * 0.5
  bodyMesh.add(cube)
  return cube
})

//添加局部相机到底盘上。它父节点是底盘，所以它的位移和旋转都是在底盘局部空间变化的。
//底盘局部相机
const tankCameraFov = 75
const tankCamera = makeCamera(tankCameraFov)
tankCamera.position.y = 3
tankCamera.position.z = -6
tankCamera.rotation.y = Math.PI
bodyMesh.add(tankCamera)

//暂时修改 渲染 底盘局部相机
renderer.render(scene, tankCamera)

//-----------绘制坦克头------------------
const domeRadius = 2
const domeWidthSubdivisions = 12
const domeHeightSubdivisions =12
const domePhiStart = 7
const domePhiEnd = Math.PI * 2
const domeThetaStart=0
const domeThetaEnd=Math.PI*0.5
const domeGeometry = new SphereGeometry(
  domeRadius,
  domeWidthSubdivisions,
  domeHeightSubdivisions,  
  domePhiStart,
  domePhiEnd,
  domeThetaStart,
  domeThetaEnd
)
const domeMesh=new Mesh(domeGeometry,bodyMaterial)
bodyMesh.add(domeMesh)
domeMesh.position.y=0.5

//炮杆
const turreWidth=0.5
const turreHeight=0.5
const turreLength=5
const turreGeometry=new BoxGeometry(turreWidth,turreHeight,turreLength)
const turreMesh=new Mesh(turreGeometry,bodyMaterial)
const turrePivot=new Object3D()
turrePivot.position.y=1.5
turreMesh.position.z=turreLength*0.5
turrePivot.add(turreMesh)
bodyMesh.add(turrePivot)

//=========绘制目标(目标是另一个在全局场景中的物体。绘制目标后我们把炮干指向目标)
const targetGeometry=new SphereGeometry(0.5,36,36)
const targetMaterial=new MeshPhongMaterial({
  color:0x00ff00,
  flatShading: true
})
const targetMesh=new Mesh(targetGeometry,targetMaterial)
const targetElevation=new Object3D()
const targetBob=new Object3D()
scene.add(targetElevation)
targetElevation.position.z=carLength*2
targetElevation.position.y=8
targetElevation.add(targetBob)
targetBob.add(targetMesh)

//获取目标全局坐标
const targetPosition=new Vector3()
targetMesh.getWorldPosition(targetPosition)
//炮台瞄准目标
turrePivot.lookAt(targetPosition)
//目标上的相机
const targetCamera=makeCamera()
targetCamera.position.y=1
targetCamera.position.z=-2
targetCamera.rotation.y=Math.PI
targetBob.add(targetCamera)


//--------绘制移动路径-----------------------------
//使用.SplineCurve()创建一个平滑的二维样条曲线
const curve=new SplineCurve([
  new Vector2(-10,20),
  new Vector2(-5,5),
  new Vector2(0,0),
  new Vector2(5,-5),
  new Vector2(10,0),
  new Vector2(5,10),
  new Vector2(-5,10),
  new Vector2(-10,-10),
  new Vector2(-15,-8),
  new Vector2(-10,20),
])
const points=curve.getPoints(50)//除法-将曲线分割成的段数.默认值为5
const geometry=new BufferGeometry().setFromPoints(points)//setFromPoints方法从points中提取数据改变几何体的顶点属性vertices
const material=new LineBasicMaterial({
  color: 0xff0000
})
const splineObject=new Line(geometry,material)
splineObject.rotation.x=Math.PI*0.5
splineObject.position.y=0.05
scene.add(splineObject)

//------------添加动画----------------------
//在循环渲染函数中，改变物体的位置使轮胎滚动就实现了坦克的移动。
//先把相机放入数组中，根据循环次数获取对应下标的相机渲染切换场景。也可以手动切换渲染相机。
//--------移动坦克-------
const targetPosition2=new Vector3()
const tankPosition=new Vector2()
const tankTarget= new Vector2()

//切换场景
const cameras=[
  {cam:camera,desc:'全局相机'},
  {cam:targetCamera,desc:'目标上的相机'},
  {cam:tankCamera,desc:'底盘 局部相机'}
]
function render(time){
  time *= 0.001
  // 开始动画
  requestAnimationFrame(render)
  //上下移动目标
  targetBob.position.y = Math.sin(time * 2) * 4
  targetMaterial.emissive.setHSL((time * 10) % 1, 1, 0.25)
  targetMaterial.color.setHSL((time * 10) % 1, 1, 0.25)
  //获取目标全局坐标
  targetMesh.getWorldPosition(targetPosition2)
  //炮杆瞄准目标
  turrePivot.lookAt(targetPosition2)

  //根据路线移动坦克
  const tankTime=time*0.05
  curve.getPointAt(tankTime % 1,tankPosition)
  //获取路径，坦克前进坐标，用于坦克头向前
  curve.getPointAt((tankTime+0.01) % 1,tankTarget)
  //位移
  tank.position.set(tankPosition.x,0,tankPosition.y)
  tank.lookAt(tankTarget.x,0,tankTarget.y)

   //切换相机
   const camera1 =cameras[time % cameras.length | 0]
   //获取坦克的全局坐标
   tank.getWorldPosition(targetPosition2)
   //看向坦克
   targetCamera.lookAt(targetPosition2)
   //加载渲染器
   renderer.render(scene,camera1.cam)
}
requestAnimationFrame(render)



