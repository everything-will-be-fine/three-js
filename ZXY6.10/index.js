import {
  Vector2,
  Raycaster,
  Object3D,
  PointLight,
  SphereGeometry,
  AxesHelper,
  Scene,
  PerspectiveCamera,
  Mesh,
  MeshBasicMaterial,
  WebGLRenderer,
  TextureLoader
} from "../node_modules/three/build/three.module.js";
import {
  OrbitControls
} from "../node_modules/three/examples/jsm/controls/OrbitControls.js";

//-------------基础模板------------------------
//创建场景
const scene = new Scene()
//创建相机
const camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 1000)
//相机位置
camera.position.set(-10, 10, 30)
camera.lookAt(0, 0, 0)
//创建渲染器
const renderer = new WebGLRenderer({
  antialias: true,
  alpha: true
})
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)
renderer.render(scene, camera)

//-----------封装函数（代表不停刷新的意思）---------------
function render(time) {
  time *= 0.001
  //太阳自转
  cube.rotation.y += 0.01
  //地球自转并绕着太阳公转
  cube2.rotation.y += 0.02
  solarSystem.rotation.y = time
  //刷新页面
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-----------创建一个纹理加载器------------------
const loader = new TextureLoader()

//---------------创建第一个模型-----------------
const geometry = new SphereGeometry(2, 36, 36)
geometry.center()
const sunTexture = loader.load('./sunText.webp')
const material = new MeshBasicMaterial({
  map: sunTexture
})
const cube = new Mesh(geometry, material)
cube.scale.set(1, 1, 1)
cube.name = 'sun'
scene.add(cube)

//----------添加辅助线-------------------
// const axes = new AxesHelper(50)
// scene.add(axes)

// //------------灯光---------------
// const light = new PointLight({
//   color: 0xffffff,
//   intensity: 1
// })
// light.position.set(0, 0, 0)
// scene.add(light)

//---------------创建第二个模型（地球）------------------
const geometry2 = new SphereGeometry(2, 36, 36)
const earthTexture = loader.load('./earth.webp')
const material2 = new MeshBasicMaterial({
  map: earthTexture
})
const cube2 = new Mesh(geometry2, material2)
cube2.position.x = 10
cube2.name = 'earth'

//---------实现地球绕着太阳转-----------------
const solarSystem = new Object3D()
solarSystem.name = 'earthAll'
scene.add(solarSystem)
solarSystem.add(cube2)
solarSystem.add(cube)



//---------鼠标控制---------------
//const controls = new OrbitControls(camera, renderer.domElement)

//----------添加点击事件----------------
const objects = []
const raycaster = new Raycaster()
const mouse = new Vector2();
//打印场景中所有几何体
console.log(scene.children[0].children)
//监听全局点击事件,通过ray检测选中哪一个object
renderer.domElement.addEventListener("click", (event) => {
  mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
  mouse.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1;
  // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
  raycaster.setFromCamera(mouse, camera);
  // 获取raycaster直线和所有模型相交的数组集合
  const intersects = raycaster.intersectObjects(scene.children[0].children);
  //   console.log(intersects)代表指到scene场景中的某个模型，返回值为数组
  //  console.log(intersects[0].object.scale.x) 

  //intersects.length>0证明点到对象了
  if (intersects.length > 0) {
    if (intersects[0].object.scale.x == 1) {
      intersects[0].object.scale.set(3, 3, 3)
    } else {
      intersects[0].object.scale.set(1, 1, 1)
    }
  } else {
    // //如果未点到(全部变为原来的样子)
    scene.children[0].children[0].scale.set(1, 1, 1)
    scene.children[0].children[1].scale.set(1, 1, 1)
  }
}, false)