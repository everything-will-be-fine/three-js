
import { OrbitControls } from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
import {Fog,Color,Mesh,PerspectiveCamera, Scene, WebGLRenderer, PlaneGeometry, Math, PointLight,AmbientLight, MeshBasicMaterial, MeshPhongMaterial, AxesHelper, DoubleSide, CircleGeometry, BoxGeometry } from "../node_modules/three/build/three.module.js";
import{Reflector}from '../node_modules/three/examples/jsm/objects/Reflector.js'

const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,1000)
camera.position.set(300,10,100)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer()
renderer.setSize(window.innerWidth,window.innerHeight)

document.body.appendChild(renderer.domElement)

function render(){
  renderer.render(scene,camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

const ambit=new AmbientLight(0xffffff)
scene.add(ambit)
const controls=new OrbitControls(camera,renderer.domElement)

const axer=new AxesHelper(50)
scene.add(axer)
scene.background=new Color('lightblue')
scene.fog=new Fog('lightblue',0.2,1000)
//------------------------创建不封顶的五面体----------------------------------
const planeGeo=new PlaneGeometry(100,100)

//顶上
const material=new MeshPhongMaterial({
  color:0x7f7fff,
  side:DoubleSide
})
const planeTop=new Mesh(planeGeo,material)
planeTop.position.y=24.8
planeTop.position.z=25
planeTop.rotateX(1.57)
scene.add(planeTop);
//底部
const planeBottom=new Mesh(planeGeo,new MeshBasicMaterial({color:0xffffff,
  side:DoubleSide}))
  planeBottom.position.y=-24.6
  planeBottom.position.z=25
  planeBottom.rotateX(1.55)
scene.add(planeBottom)
//前面
const planeFront=new Mesh(planeGeo,new MeshBasicMaterial({color:0x7f7fff,
  side:DoubleSide}))
  planeFront.position.y=0
  planeFront.position.z=0
scene.add(planeFront)
//右面
const planeRight=new Mesh(planeGeo,new MeshBasicMaterial({color:0x00ff00,
  side:DoubleSide}))
planeRight.position.x=0;
planeRight.position.y=0;
planeRight.position.z=50
scene.add(planeRight)

//左边
const planeLeft=new Mesh(planeGeo,new MeshBasicMaterial({color:0xff0000,side:DoubleSide}))
planeLeft.position.x=-25
planeLeft.position.z=25
planeLeft.rotateY(1.57)
scene.add(planeLeft)

//----------------创建需要被映射的物体--------------------
const box=new BoxGeometry(10,10,10)
const boxmaterial=new MeshBasicMaterial({color:0x1E90FF})
const cube=new Mesh(box,boxmaterial)
cube.position.z=20
scene.add(cube)

//创建圆形水平镜面
const geometry=new CircleGeometry(40,64)
const groundMirror=new Reflector(geometry,{
  clipBias:0.003,
  textureWidth:window.innerWidth*window.devicePixelRatio,
  textureHeight:window.innerHeight*window.devicePixelRatio,
  color:0x777777
})
groundMirror.position.y=0.5
groundMirror.position.z=5
scene.add(groundMirror);


