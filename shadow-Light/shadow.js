import {DirectionalLightHelper,PerspectiveCamera,WebGLRenderer, Scene, DirectionalLight, BoxGeometry, MeshPhongMaterial, Mesh, PlaneGeometry, DoubleSide, CameraHelper } from "../node_modules/three/build/three.module.js";
import { OrbitControls } from "../node_modules/three/examples/jsm/controls/OrbitControls.js";
const scene=new Scene()
const camera=new PerspectiveCamera(40,window.innerWidth/window.innerHeight,0.1,1000)
camera.position.set(0,50,50)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer()
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls=new OrbitControls(camera,renderer.domElement)
controls.update()

//不停渲染
function render(){
  renderer.render(scene,camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-------------开启阴影------------------
renderer.shadowMap.enabled=true;

//添加灯光开启投射阴影
const color=0xffffff
const intensity=2
const light=new DirectionalLight(color,intensity)
//投射阴影
light.castShadow=true
light.position.set(10,10,10)
light.target.position.set(-4,0,-4)
scene.add(light)
scene.add(light.target)
//光源助手
const helper=new DirectionalLightHelper(light)
scene.add(helper)

//------绘制物体------------
//平面几何
const planeGeometry=new PlaneGeometry(50,50)
const planeMaterial=new MeshPhongMaterial({color:0xcc8866})
const plane=new Mesh(planeGeometry,planeMaterial)
plane.rotation.x=Math.PI*-0.5
plane.receiveShadow=true
scene.add(plane)

//几何体
const cubeSize=4
const geometry=new BoxGeometry(cubeSize,cubeSize,cubeSize)
const material=new MeshPhongMaterial({color:'#8AC'})
const mesh=new Mesh(geometry,material)
mesh.position.set(cubeSize+1,cubeSize/2,0)
mesh.castShadow=true//投射阴影
scene.add(mesh)

//从结果中看，阴影少了一部分。是因为光源阴影相机决定了阴影投射的区域
//使用.CameraHelper(light.shadow.camera)来获取光源的阴影相机的辅助线。
const cameraHelper=new CameraHelper(light.shadow.camera)
scene.add(cameraHelper)

//设置阴影相机
const d=50;
light.shadow.camera.left=-d
light.shadow.camera.right=d
light.shadow.camera.top=d
light.shadow.camera.bottom=-d
light.shadow.camera.near=1
light.shadow.camera.far=60