//1,创建场景
var scene = new THREE.Scene();

//2,创建相机
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

//3,创建渲染器
var renderer = new THREE.WebGL1Renderer()
//设置渲染器大小（设置渲染区域的大小）
renderer.setSize(window.innerWidth, window.innerHeight);
//把创造好的canvas挂载到指定的dom上面去
document.body.appendChild(renderer.domElement);
//把场景和摄像机放入到渲染器当中去
renderer.render(scene, camera)

//4,创建一个几何体(正方体)
var geometry = new THREE.BoxGeometry(2, 2, 2);
//.toJSON()通过这个方法可以导出Threejs三维模型的各类数据
//该方法的功能就是把Threejs的几何体、材质、光源等对象转化为JSON格式导出
console.log(geometry.toJSON())
console.log(JSON.stringify(geometry.toJSON()))
console.log(JSON.stringify(geometry))
//创建一个材料，并设置颜色
var material = new THREE.MeshBasicMaterial({
  color: 0x002299
});
//创建网格
const cube = new THREE.Mesh(geometry, material);
//网格的位置
cube.rotation.x = 0.3;
cube.rotation.y = 0.5;
//将网格放入到渲染器当中
scene.add(cube)

//设置一下相机的位置
camera.position.z = 10;

//重新渲染一下
renderer.render(scene, camera)

//导出场景信息
console.log(scene.toJSON())

//设置动态
//方法一:周期性渲染
// function render(){
//   renderer.render(scene,camera)//执行渲染操作
//   cube.rotateY(0.01)//每次绕y轴旋转0.01弧度
//   cube.rotateX(0.01)//每次绕y轴旋转0.01弧度
// }
// //间隔10ms周期性调用函数fun,10ms也就是刷新频率是100fps(1s/10ms,)每秒渲染100次
// setInterval("render()",10)

// //方法二:渲染频率(向浏览器发起请求, 什么时候会执行由浏览器决定，一般默认保持60FPS的频率)
// function render(){
//   renderer.render(scene,camera)
//   cube.rotateY(0.01)
//   cube.rotateX(0.01)
//   requestAnimationFrame(render)
// }
// render()

// // 均匀旋转
// let T0 = new Date();//上次时间
// function render() {
//         let T1 = new Date();//本次时间
//         let t = T1-T0;//时间差
//         T0 = T1;//把本次时间赋值给上次时间
//         requestAnimationFrame(render);
//         renderer.render(scene,camera);//执行渲染操作
//         cube.rotateY(0.001*t);//旋转角速度0.001弧度每毫秒
//         cube.rotateX(0.001*t);//旋转角速度0.001弧度每毫秒
// }
// render();

//绘制球体模型(radius,widthSegments,heightSegments)
//1,第一个参数是半径,2,3为球面精度
//重新设计一个scene camera
const boll=new THREE.SphereGeometry(2,6,6)
const material2=new THREE.MeshBasicMaterial({color:0x002299})
const mesh2=new THREE.Mesh(boll,material2)
mesh2.position.set(10,0,0);
scene.add(mesh2);
renderer.render(scene, camera)


// //利用鼠标控制大小
// //(控制鼠标左中右键操作和键盘方向键操作)
// //创建控件对象
// function render() {
//   renderer.render(scene, camera)
//   requestAnimationFrame(render);
// }
// render()
// var controls = new THREE.OrbitControls(camera, renderer.domElement);

