import {
  Group,
  Scene,
  MeshBasicMaterial,
  PerspectiveCamera,
  WebGL1Renderer,
  Mesh,
  AmbientLight,
} from '../node_modules/three/build/three.module.js'
import {
  StereoEffect
} from '../node_modules/three/examples/jsm/effects/StereoEffect.js'
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'

import{STLLoader}from'../node_modules/three/examples/jsm/loaders/STLLoader.js'


//创建一个场景
var scene = new Scene();
//创建相机
const camera=new PerspectiveCamera(40,window.innerWidth/window.innerHeight,0.1,1000)
camera.position.set(0,0,10)
camera.lookAt(0,0,0)

//创建渲染器
const renderer=new WebGL1Renderer()
renderer.setSize(window.innerWidth,window.innerHeight)
//设置大小,颜色
renderer.setClearColor(0xffffff, 0.5)
//挂载dom
document.body.appendChild(renderer.domElement)
renderer.render(scene,camera)

//创建一个实例化对象
var VrRender=new StereoEffect(renderer)
//设置渲染器恢复默认状态
// var size = renderer.getSize();
// renderer.setViewport(0, 0, size.width, size.height);
VrRender.render(scene, camera)
//创建组
const group=new Group()
scene.add(group)


//-------------------加载stl文件---------------------------------
//引入一个扩展库stl加载器STLLoader.js
//构造函数创建一个加载器
var loader = new STLLoader();
loader.load('./门静脉血管.stl', geometry => {
  //缩放几何体
  geometry.scale(0.02, 0.02, 0.02);
  //几何体居中
  geometry.center();
  //设置材料信息
  const material = new MeshBasicMaterial({
    color: 0xff0000
  })
 //创建网格
  const cube = new Mesh(geometry, material)
  group.add(cube);
})

//产生环境光
const ambientLight=new AmbientLight(0xaaaaaa)
scene.add(ambientLight)

function render() {
  group.rotation.y+=0.02
  VrRender.render(scene, camera)
  requestAnimationFrame(render)
}
render()
controls = new OrbitControls(camera, renderer.domElement);


