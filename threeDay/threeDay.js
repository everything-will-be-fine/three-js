//引入一个扩展库stl加载器
var loader = new THREE.STLLoader();
loader.load('../work/pic/部分肠管.stl', geometry => {
  console.log(geometry);
  //创建场景
  const scene = new THREE.Scene();
  //创建相机
  var width = window.innerWidth;
  var height = window.innerHeight;
  var k = width / height;
  var s = 50;
  var camera = new THREE.OrthographicCamera(-s * k, s * k, s, -s, -2000, 2000);
  camera.position.set(100, 100, 120);
  camera.lookAt(scene.position);
  // const camera=new THREE.PerspectiveCamera(75,window.innerWidth / window.innerHeight,0.1,1000)
  //创建渲染器
  //方法一:确定需要绑定的对象
  // const canvas =document.getElementById('big')
  // const renderer= new THREE.WebGL1Renderer({canvas:canvas});

  //方法二:
  const renderer = new THREE.WebGL1Renderer();
  //渲染大小
  renderer.setSize(800, 800)
  // renderer.setClearColor(0xeeeeee, 0.5)
  //挂载dom
  document.getElementById('big').appendChild(renderer.domElement)

  //把相机和场景渲染进去
  renderer.render(scene, camera)
  //添加材料
  const materia = new THREE.MeshBasicMaterial({
    color: 0x002299
  })
  geometry.center();
  geometry.scale(1, 1, 1)
  //创建网格
  const cube = new THREE.Mesh(geometry, materia)
  //网格的位置
  // cube.rotation.x=0.15;
  // cube.rotation.y=0.5;
  //将网格放入到渲染器当中
  scene.add(cube)
  renderer.render(scene, camera)
  console.log(scene.toJSON())
  //自己旋转
  function animate() {
    requestAnimationFrame(animate)
    cube.rotation.x += 0.02;
    cube.rotation.y += 0.02;
    renderer.render(scene, camera)
  }
  animate()
})
//第二个
//引入一个扩展库stl加载器
var loader2 = new THREE.STLLoader();
loader2.load('../work/pic/部分胃.stl', geometry => {
  //创建场景
  const scene = new THREE.Scene();
  //创建相机
  var width = window.innerWidth;
  var height = window.innerHeight;
  var k = width / height;
  var s = 50;
  var camera = new THREE.OrthographicCamera(-s * k, s * k, s, -s, -2000, 2000);
  camera.position.set(100, 100, 120);
  camera.lookAt(scene.position);
  // const camera=new THREE.PerspectiveCamera(75,window.innerWidth / window.innerHeight,0.1,1000)
  //创建渲染器
  //方法一:确定需要绑定的对象
  // const canvas =document.getElementById('big')
  // const renderer= new THREE.WebGL1Renderer({canvas:canvas});

  //方法二:
  const renderer = new THREE.WebGL1Renderer();
  //渲染大小
  renderer.setSize(800, 800)
  // renderer.setClearColor(0xeeeeee, 0.5)
  //挂载dom
  document.getElementById('big2').appendChild(renderer.domElement)

  //把相机和场景渲染进去
  renderer.render(scene, camera)
  console.log(geometry);
  const materia = new THREE.MeshBasicMaterial({
    color: 0xff0000
  })
  geometry.center();
  geometry.scale(1, 1, 1)
  //创建网格
  const cube = new THREE.Mesh(geometry, materia)
  //网格的位置
  // cube.rotation.x=0.15;
  // cube.rotation.y=0.5;
  //将网格放入到渲染器当中
  scene.add(cube)
  renderer.render(scene, camera)
  console.log(scene.toJSON())
  //自己旋转
  // function animate(){
  //   requestAnimationFrame(animate)
  //   cube.rotation.x+=0.02;
  //   cube.rotation.y+=0.02;
  //   renderer.render(scene,camera)
  // }
  // animate()
  function render() {
    renderer.render(scene, camera)
    requestAnimationFrame(render)
    // cube.rotation.x+=0.02;
    // cube.rotation.y+=0.02;
  }
  render()
  controls = new THREE.OrbitControls(camera, renderer.domElement)
})