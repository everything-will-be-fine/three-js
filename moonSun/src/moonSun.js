import {
  MeshPhongMaterial,
  MeshBasicMaterial,
  Mesh,
  SphereGeometry,
  WebGLRenderer,
  PerspectiveCamera,
  Scene,
  TextureLoader,
  PointLight,
  AxesHelper,
  Object3D
} from '../../node_modules/three/build/three.module.js';
import {
  OrbitControls
} from '../../node_modules/three/examples/jsm/controls/OrbitControls.js'
import{CSS2DObject, CSS2DRenderer}from '../../node_modules/three/examples/jsm/renderers/CSS2DRenderer.js'
//创建初始状态
//创建场景
const scene = new Scene()
//创建相机
const camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.set(0, 0, 50)
camera.lookAt(0, 0, 0)
//创建渲染器
const renderer = new WebGLRenderer({
  antialias: true, //开启锯齿
  alpha: true //透明度（这个属性至关重要）
})

//--------------创建CSS2DRenderer渲染器---------------------
const labelRenderer=new CSS2DRenderer()
labelRenderer.setSize(window.innerWidth,window.innerHeight)
labelRenderer.domElement.style.position='absolute'
labelRenderer.domElement.style.top='0px'
document.body.appendChild(labelRenderer.domElement)

//----------------普通渲染器----------------------------
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

renderer.render(scene, camera)


//控制相机
const controls=new OrbitControls(camera,labelRenderer.domElement)


//场景不停刷新
function render(time) {
  time *= 0.001
  // 加载渲染器
  renderer.render(scene, camera)
  //太阳旋转
  objects.forEach((obj)=>{
    obj.rotation.y=time
  })
  // 开始动画
  labelRenderer.render(scene,camera)
  requestAnimationFrame(render)
}
// 开始渲染
requestAnimationFrame(render)
//纹理加载器
const loader = new TextureLoader()

//创建光源
const light = new PointLight({
  color: 0xffffff,
  intensity: 1 // 强度
})
scene.add(light)
renderer.render(scene, camera)


//添加太阳 （物体网格对象）
const objects = [];
const sunText = new SphereGeometry(2, 36, 36)
sunText.center()
const sunTexture = loader.load('../images/sunText.webp')
const materia = new MeshBasicMaterial({
    map: sunTexture,
})
const sunMesh = new Mesh(sunText, materia)
//放大三倍
sunMesh.scale.set(3, 3, 3)
//scene.add(sunMesh)
objects.push(sunMesh)
renderer.render(scene, camera)

//------------------------添加太阳div----------------------------------------
const sunDiv=document.createElement('div')
sunDiv.className='label'
sunDiv.textContent='太阳'
sunDiv.style.color='white'
sunDiv.style.background='none'
const sunLabel=new CSS2DObject(sunDiv)
sunLabel.position.set(0,2.5,0)
sunMesh.add(sunLabel)


// 创建一个球几何体，太阳、地球、月亮都是球形，我们可以公用一个球体。
// 使用基础材质加载太阳纹理。因为灯光是点光源，发光点在中心，太阳也在中心，使用其他材质是无法接收光源。
// 太阳比其他球体大，放大3倍。

//添加地球
const earthText=loader.load('../images/earth.webp')
const earthMaterial=new MeshPhongMaterial({
  map:earthText
})
const earthMesh=new Mesh(sunText,earthMaterial)
//earthMesh.position.x=20
//scene.add(earthMesh)
//放入控制对象(实现太阳自转)
objects.push(earthMesh)

//这里我们添加一个新场景太阳系，把地球和太阳都放入场景中，旋转太阳系这个场景，因为太阳在中心，实现的效果就是地球围绕太阳转。
//创建新场景，太阳系
const solarSystem=new Object3D()
scene.add(solarSystem)//显示出来
objects.push(solarSystem)//能够整体旋转
solarSystem.add(sunMesh)//添加太阳
//solarSystem.add(earthMesh)//添加地球

//----------------------添加地球Div--------------------------------
const earthDiv=document.createElement('div')
earthDiv.className='label'
earthDiv.textContent='地球'
earthDiv.style.color='white'
earthDiv.style.background='none'
const earthLabel=new CSS2DObject(earthDiv)
earthLabel.position.set(0,3,0)
earthMesh.add(earthLabel)

//添加月球
//同样的月球要围绕地球转，添加一个地月系到太阳系中旋转
const landOrbit=new Object3D()
landOrbit.position.x=20
solarSystem.add(landOrbit)
objects.push(landOrbit)

//定位地月系位置到原来地球的位置放入太阳系中，取消地球的定位和取消放入太阳系中。
const moonTexture=loader.load('../images/moon2.webp')
const moonMaterial=new MeshPhongMaterial({
  map:moonTexture
})
const moonMesh= new Mesh(sunText,moonMaterial)
moonMesh.scale.set(0.5,0.5,0.5)
moonMesh.position.x=5
objects.push(moonMesh)//月球自转

//----------------------添加月球Div--------------------------------
const moonDiv=document.createElement('div')
moonDiv.className='label'
moonDiv.textContent='月球'
moonDiv.style.color='white'
moonDiv.style.background='none'
const moonLabel=new CSS2DObject(moonDiv)
moonLabel.position.set(0,3,0)
moonMesh.add(moonLabel)

//加入地月系
landOrbit.add(earthMesh)
landOrbit.add(moonMesh)