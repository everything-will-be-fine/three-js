import {
  Scene,
  PerspectiveCamera,
  Vector2,
  SpotLight,
  WebGL1Renderer,
  Mesh,
  BoxGeometry,
  AmbientLight,
  MeshLambertMaterial,
  PlaneGeometry
} from '../node_modules/three/build/three.module.js'
import {
  StereoEffect
} from '../node_modules/three/examples/jsm/effects/StereoEffect.js'
import {
  OrbitControls
} from '../node_modules/three/examples/jsm/controls/OrbitControls.js'
//创建场景
const scene = new Scene();
//创建摄像机
const camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
//将摄像机方向
// camera.position.x=30;
// camera.position.y=15;
// camera.position.z=35;
camera.position.set(0, 400, 500)
//把摄像机的方向对准场景的中心点
camera.lookAt(scene.position)
//创建渲染器
const renderer = new WebGL1Renderer();
// renderer.setClearColor(0xeeeeee, 0.5)
renderer.setSize(window.innerWidth, window.innerHeight)
//渲染器阴影
renderer.shadowMap.enabled = true;
document.body.appendChild(renderer.domElement)
renderer.render(scene, camera)

//添加三维轴(调整线的长段)
// const axes=new AxesHelper(50);
// scene.add(axes)
//修改轴的颜色
// axes.setColors(0x00ffff,0x002299,0xff0000)
//创建正方体
const geometry = new BoxGeometry(8, 8, 8)
geometry.center();
//创建材质
const materia = new MeshLambertMaterial({
  color: 0x002299
})
//创建网格
const cube = new Mesh(geometry, materia)
cube.castShadow = true;
cube.rotation.x = 0.8;
cube.rotation.y = 0.9;
cube.position.x = 10;
cube.position.y = 10;
cube.position.z = 10;
//将网格放入场景当中
scene.add(cube)

//创建一个平面几何体
const planeGeometry = new PlaneGeometry(100, 100)
//设置场面材质(因为要观察反射光，所以不能用基础材质)
const planeMaterial = new MeshLambertMaterial({
  color: 0xcccccc
});
//平面网格
const plane = new Mesh(planeGeometry, planeMaterial)
//平面接受阴影的shadow为真
plane.receiveShadow = true;
plane.rotation.x = -0.5 * Math.PI;
plane.position.set(15, 0, 0)
scene.add(plane)


//采用MeshLambertMaterial，需要有光源才行(环境光，没有确定的方向，所有没有阴影)
const ambientLight = new AmbientLight(0xaaaaaa);
scene.add(ambientLight);
renderer.render(scene, camera)

//产生阴影，需要有聚光灯
const spotLight = new SpotLight(0x00FA9A)
spotLight.position.set(-60, 40, -65)
//开启阴影效果
spotLight.castShadow = true;
//设置阴影效果
spotLight.shadow.mapSize = new Vector2(1024, 1024);
spotLight.shadow.camera.far = 130;
spotLight.shadow.camera.near = 40;
scene.add(spotLight)
renderer.render(scene, camera)

//创建球体
// const geometry2=new SphereGeometry(6,6,6)
// const materia2=new MeshLambertMaterial({
//   color:0xff0000,
//   opacity:0.5,
//   transparent:true
// })
// const cube2=new Mesh(geometry2,materia2)

// cube2.translateX(20)
// scene.add(cube2)
// renderer.render(scene,camera)

//设置旋转
// function animat(){
//   renderer.render(scene,camera)
//   requestAnimationFrame(animat)
//   cube.rotation.y+=0.02
// }
// animat()



//实现双屏展示
const VrRender = new StereoEffect(renderer)
//设置渲染器恢复默认状态
var size = renderer.getSize();
renderer.setViewport(0, 0, size.width, size.height);
//VrRender.setSize(window.innerWidth,window.innerHeight)
VrRender.render(scene, camera)
//在渲染场景时，我们直接改成双屏对象进行渲染即可
 function animate(){
      cube.rotation.x+=0.02;
      cube.rotation.y+=0.02;
      requestAnimationFrame(animate)
      VrRender.render(scene, camera)
    }
    animate()
//控制鼠标
function render() {
  VrRender.render(scene, camera)
  requestAnimationFrame(render)
}
render()
const controls = new OrbitControls(camera, renderer.domElement)