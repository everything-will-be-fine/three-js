
import { PerspectiveCamera,DirectionalLight, Scene, WebGLRenderer, BoxGeometry, MeshPhongMaterial, Mesh, Raycaster } from "../node_modules/three/build/three.module.js"
import{OrbitControls} from'../node_modules/three/examples/jsm/controls/OrbitControls.js'
//---------------------基础模板---------------------------------
const canvas=document.querySelector('#c2d')
const scene=new Scene()
const camera=new PerspectiveCamera(40,window.innerWidth/window.innerHeight,0.1,1000)
camera.position.set(0,6,5)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer({
  canvas,
  antialias: true, //开启锯齿
})
renderer.setSize(window.innerWidth,window.innerHeight)
//const controls=new OrbitControls(camera,renderer.domElement)

//------------------封装-------------------
function render(time){
  time*=0.05
  renderer.render(scene,camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)

//-----------灯光----------------
const color=0xffffff
const intensity=1
const light=new DirectionalLight(color,intensity)
light.position.set(-1,10,4)
scene.add(light)

//---------------几何体-------------------
const box=1
const Geometry=new BoxGeometry(box,box,box)
const Material=new MeshPhongMaterial({
  color:0x6688aa
})
const cube=new Mesh(Geometry,Material)
cube.position.x=-1
scene.add(cube)

const Material2=new MeshPhongMaterial({
  color:0x668866
})
const cube2=new Mesh(Geometry,Material2)
cube2.position.x=1
scene.add(cube2)

//------------使用.Raycaster()----------------------------
// 计算 以画布 开始为（0，0）点 的鼠标坐标
function getCanvasRelativePosition(event){
  const rect=canvas.getBoundingClientRect()
  return{
    x:((event.clientX-rect.left)*canvas.width)/rect.width,
    y:((event.clientY-rect.top)*canvas.height)/rect.height
  }
}
//获取鼠标在three.js 中归一化坐标
function setPickPosition(event){
  let pickPosition={x:0,y:0}
   // 计算后 以画布 开始为 （0，0）点
   const pos=getCanvasRelativePosition(event)
   //数据归一化
   pickPosition.x=(pos.x/canvas.width)*2-1
   pickPosition.y=(pos.y/canvas.height)*-2+1
   return pickPosition
}
//监听鼠标
window.addEventListener('mousemove',onRay)
//全局对象
let lastPick=null
function onRay(event){
  let pickPosition=setPickPosition(event)
  const raycaster=new Raycaster()
  console.log(raycaster)
  raycaster.setFromCamera(pickPosition,camera)
  //计算物体和射线的交点(点击的物体与场景里所有物体的交点)
  const intersects=raycaster.intersectObjects(scene.children,true)
  console.log(intersects)
  //数组大于0，表示有相交对象
  if(intersects.length>0){
    if(lastPick){
      lastPick.object.scale.set(1.5,1.5,1.5)
    }
    lastPick=intersects[0]
  }else{
    if(lastPick){
      //复原
      lastPick.object.scale.set(1,1,1)
      lastPick=null
    }
  }
}