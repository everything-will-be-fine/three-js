import { OrbitControls } from '../node_modules/three/examples/jsm/controls/OrbitControls.js';
import {Mesh,CylinderGeometry, DoubleSide, MeshBasicMaterial, PerspectiveCamera, RepeatWrapping, Scene,TextureLoader, WebGLRenderer } from '../node_modules/three/build/three.module.js';

const scene=new Scene()
const camera=new PerspectiveCamera(40,2,0.1,1000)
camera.position.set(0,10,20)
camera.lookAt(0,0,0)
const renderer=new WebGLRenderer({
  alpha:true
})
renderer.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls=new OrbitControls(camera,renderer.domElement)


//创建圆柱
const geometry=new CylinderGeometry(4,4,4,64)
//加载纹理
const text=new TextureLoader().load('./images/circle.webp')
text.wrapS=text.wrapT=RepeatWrapping;//每个都重复
text.repeat.set(1,1)
text.needsUpdate=true

let materials=[
  //圆柱侧面材质，使用纹理贴图
  new MeshBasicMaterial({
    map:text,
    side:DoubleSide,
    transparent:true
  }),
  //圆柱顶材质
  new MeshBasicMaterial({
    transparent:true,
    opacity:0,
    side:DoubleSide
  }),
  //圆柱底材质
  new MeshBasicMaterial({
    transparent:true,
    opacity:0,
    side:DoubleSide
  })
]
const mesh=new Mesh(geometry,materials)
scene.add(mesh)
console.log(mesh)

//圆柱扩散
let cylinderRadius=0
let cylinderOpacity=1
function render(){
  cylinderRadius+=0.01
  cylinderOpacity-=0.003
  if(cylinderRadius>4){
    //恢复原状
    cylinderRadius=0
    cylinderOpacity=1
  }
  if(mesh){
    mesh.scale.set(1+cylinderRadius,1,1+cylinderRadius)//圆柱半径增大
    mesh.material[0].opacity=cylinderOpacity
  }
  renderer.render(scene,camera)
  requestAnimationFrame(render)
}
requestAnimationFrame(render)
